#pragma once
#include "network_node.h"

/*!
 * \brief NetworkConnection
 * Модель подключения между двумя узлами
 */
class NetworkConnection
{
    /*!
     * \brief first
     * Первый узел, участвующий в соединении
     */
    NetworkNode *first;
    /*!
     * \brief second
     * Второй узел, участвующий в соединении
     */
    NetworkNode *second;
    /*!
     * \brief active
     * Активность сетевого соединения
     */
    bool active;
    /*!
     * \brief busy
     * Флаг занятости соединения передачей данных
     */
    bool busy;

    /*!
     * \brief speed
     * Скорость соединения (байт / с)
     */
    unsigned speed;

    public:
    /*!
     * \brief NetworkConnection
     * Параметризованный конструктор для создания сетевого соединения между
     * двумя узлами
     * \param fst Первый подключаемый узел
     * \param snd Второй подключаемый узел
     * \param act Начальное состояние соединения (активно / неактивно)
     */
    NetworkConnection(NetworkNode *fst, NetworkNode *snd, bool act=false)
    {
        speed = 1;
        first = fst;
        second = snd;
        if (first > second) swap();
        active = act;
        busy = false;
    }
    /*!
     * \brief remove_self
     * Оборвать данное соединение
     */

    /*!
     * \brief operator ==
     * Перегрузка оператора сравнения
     * \param c
     * Соединение, с которым выполняется сравнение
     * \return Результат сравнения
     */
    inline bool operator==(const NetworkConnection &c) const
    {
        return first == c.first && second == c.second;
    }
    /*!
     * \brief fst
     * Получение первого узла, участвующего в данном соединении
     * \return
     */
    NetworkNode* fst() const
    {
        return first;
    }

    /*!
     * \brief speed
     * Получить скорость соединения
     */
    unsigned getSpeed() const {
        return speed;
    }

    /*!
     * \brief setSpeed
     * Установить скорость соединения
     * \param spd Скорость соединения
     */
    void setSpeed(unsigned spd) {
        speed = spd;
    }

    /*!
     * \brief snd
     * Получение второго узла, участвующего в данном соединении
     * \return
     */
    NetworkNode* snd() const
    {
        return second;
    }
    /*!
     * \brief isActive
     * Статус соединения (активно / неактивно)
     * \return
     */
    inline bool isActive() const
    {
        return active;
    }
    /*!
     * \brief swap
     * Обменять первый и второй узлы в соединении местами
     */
    void swap()
    {
        NetworkNode *t = first;
        first = second;
        second = t;
    }
    /*!
     * \brief activate
     * Установить соединение активным
     */
    inline void activate()
    {
        active = true;
    }
    /*!
     * \brief deactivate
     * Установить соединение неактивным
     */
    inline void deactivate()
    {
        active = false;
    }
    /*!
     * \brief take
     * Установить соединение занятым
     */
    inline void take()
    {
        busy = true;
    }
    /*!
     * \brief free
     * Освободить соединение для передачи данных
     */
    inline void free(bool free_nodes = true)
    {
        busy = false;
        if (free_nodes && first != nullptr) first->free();
        if (free_nodes && second != nullptr) second->free();
    }
    /*!
     * \brief isBusy
     * Проверить занятость соединения
     * \return
     */
    inline bool isBusy() const
    {
        return busy;
    }

    /*!
     * \brief ~NetworkConnection
     * Деструктор класса NetworkConnection (используется для отладки)
     */
    ~NetworkConnection()
    {
        //QMessageBox::warning(NULL, " ", " ");
    }
};
