#pragma once
#include <QMessageBox>
#include "network_agent.h"
#include "network_model.h"
#include <vector>
#include "v2f.h"

class NetworkConnection;

/*!
 * \brief NetworkNode
 * Класс, описывающий поведение узла сети.
 */
class NetworkNode
{
    /*!
     * \brief agent
     * Сетевой агент, реализующий протокол
     */
    NetworkAgent agent;
    bool isSending = false;
    bool isReceiving = false;


    /*!
     * \brief center
     * Обозначает главный узел сети
     */
    bool center = false;
    /*!
     * \brief source
     * Флаг узла-источника
     */
    bool source = false;
    /*!
     * \brief dest
     * Флаг принимающего узла
     */
    bool dest = false;

    /*!
     * \brief num
     * Логический номер узла в сети
     */
    unsigned num;
    /*!
     * \brief pos
     * Физическое расположение узла
     */
    v2f pos;
    /*!
     * \brief spd
     * Скорость перемещения узла
     */
    v2f spd;

public:


    /*!
     * \brief inp_data
     * Указатель на получаемые данные
     */
    void *inp_data;
    /*!
     * \brief out_data
     * Указатель на передаваемые данные
     */
    void *out_data;
    /*!
     * \brief inp_size
     * Размер буфера получаемых данных
     */
    unsigned inp_size;
    /*!
     * \brief out_size
     * Размер буфера передаваемых данных
     */
    unsigned out_size;
    /*!
     * \brief inp_addr
     * Узел, от которого были получены данные
     */
    unsigned inp_addr;
    /*!
     * \brief out_addr
     * Узел, которому выполняется отправка данных
     */
    unsigned out_addr;

    // --- ------  ---

    /*!
     * \brief time
     * Время работы модели
     */
    float *time;

    /*!
     * \brief recv
     * Моделирует получение данных узлом
     * \param snd Логический номер узла, от которого поступили данные
     * \param dt Указатель на буфер получаемых данных
     * \param sz Размер буфера в байтах
     */
    void recv(unsigned snd, void *dt, unsigned sz)
    {
        inp_addr = snd;
        inp_data = dt;
        inp_size = sz;
    }

    /*!
     * \brief NetworkNode
     * Параметризованный конструктор узла сети.
     * \param _pos Координаты создаваемого узла
     * \param n Логический номер узла в сети
     * \param tm Указатель на время работы модели
     */
    NetworkNode(v2f _pos = {0.0, 0.0}, unsigned n=0, float *tm = nullptr):
        agent(), inp_data(nullptr), out_data(nullptr) {
        pos = _pos;
        spd = v2f::zero;
        num = n;
        agent.number = num;
        agent.pos = &pos;
        agent.spd = &spd;
        time = tm;
        agent.time = tm;
        agent.initTable();

    }

    /*!
     * \brief position
     * \return Позиция узла в пространстве
     */
    const v2f& position() const
    {
        return pos;
    }

    /*!
     * \brief position
     * Изменить расположение узла в пространстве
     * \param p Назначаемое расположение
     */
    void position(const v2f &p)
    {
        pos = p;
    }

    /*!
     * \brief speed
     * \return Скорость движения узла
     */
    const v2f& speed() const
    {
        return spd;
    }

    /*!
     * \brief number
     * \return Логический адрес узла в сети
     */
    unsigned number() const
    {
        return num;
    }

    /*!
     * \brief number
     * Задать логический адрес узла в сети
     * \param new_number Задаваемый адрес
     */
    void number(unsigned new_number)
    {
        num = new_number;
    }

    /*!
     * \brief step
     * Выполнить шаг моделирования
     * \param factor Временной отрезок моделирования
     */
    void step(float factor)
    {
        pos += spd * factor;

        // push buffers
        if (inp_data != nullptr)
        {
            agent.inp_data = inp_data;
            agent.inp_size = inp_size;
            agent.inp_addr = inp_addr;
            inp_data = nullptr;
        }

        agent.step(factor);

        // get response if avail
        if (agent.out_data != nullptr)
        {
            out_data = agent.out_data;
            out_addr = agent.out_addr;
            out_size = agent.out_size;
            agent.out_data = nullptr;
        }
    }



    /*!
     * \brief take
     * Заблокировать узел для обмена данными
     */
    void take()
    {
        //busy = true;

    }

    void setSending(bool state = true) {
        isSending = state;
        agent.setSending(state);
    }

    void setReceiving(bool state = true) {
        isReceiving = state;
        agent.setReceiving(state);
    }

    inline bool canSend() const {
        return !isSending;
    }

    inline bool canReceive() const {
        return !isReceiving;
    }

    /*!
     * \brief free
     * Освободить узел для обеспечения возможности обмена данными
     */
    void free()
    {
        //busy = false;

    }


    /*!
     * \brief setCentral
     * Задать узел как главный
     */
    inline void setCentral()
    {
        center = true;
    }

    /*!
     * \brief setSource
     * Назначить узел передающим данные
     * \param src Является ли узел передающим
     */
    inline void setSource(bool src = true) {
        source = src;
    }

    /*!
     * \brief setDest
     * Назначить узел принимающим данные
     * \param dst Является ли узел принимающим
     */
    inline void setDest(bool dst = true) {
        dest = dst;
    }

    /*!
     * \brief resetCentral
     * Снять с узла статус главного
     */
    inline void resetCentral()
    {
        center = false;
    }

    /*!
     * \brief isCentral
     * \return Статус главного узла
     */
    inline bool isCentral()
    {
        return center;
    }

    /*!
     * \brief isSource
     * \return Статус передающего узла
     */
    inline bool isSource() {
        return source;
    }

    /*!
     * \brief isDest
     * \return Статус принимающего узла
     */
    inline bool isDest() {
        return dest;
    }

    /*!
     * \brief getAgent
     * \return Сетевой агент узла, реализующий протокол
     */
    NetworkAgent &getAgent() {
        return agent;
    }
};
