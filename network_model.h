#pragma once
#include <QMessageBox>
#include <functional>
#include "network_node.h"
#include "network_connection.h"
#include "network_package.h"
#include "v2f.h"
#include "v2i.h"
#include "utility.h"
#include <vector>
#include <ctime>

const byte NO_NODE = 0xff;

class NetworkNode;

/*!
 * \brief NetworkModel
 * Класс, моделирующий сеть
 */
class NetworkModel
{


    /*!
     * \brief fps
     * Количество шагов моделирования, выполняемое в секунду
     */
    unsigned fps;
    /*!
     * \brief bps
     * Скорость передачи данных (байт / с)
     */
    unsigned bps;
    /*!
     * \brief time
     * Время моделирования
     */
    float time;
    /*!
     * \brief nodes_v
     * Список узлов сети
     */
    std::vector<NetworkNode*> nodes_v;
    /*!
     * \brief links
     * Список сетевых соединений
     */
    std::vector<NetworkConnection> links;
    /*!
     * \brief pkgs
     * Список пакетов, передаваемых по сети в текущий момент времени
     */
    std::vector<NetworkPackage> pkgs;

    int source = -1;
    int dest = -1;
    int center = -1;

public:

    /*!
     * \brief onPkgSent
     * Функция, вызываемая при отправке пакета
     */
    std::function<void(void *np)> onPkgSent;
    /*!
     * \brief onPkgDest
     * Функция, вызываемая при получении пакета
     */
    std::function<void(float t, byte type, byte from, byte to)> onPkgDest;

    /*!
     * \brief NetworkModel
     * Параметризованный конструктор модели
     * \param _fps Количество шагов моделирования, выполняемое в единицу времени (секунду)
     * \param b_per_sec Скорость сетевого соединения (байт / с)
     */
    NetworkModel(unsigned _fps=1000, unsigned b_per_sec=128):
        fps(_fps),
        bps(b_per_sec),
        time(0.0f) {

        nodes_v.reserve(20);
        links.reserve(50);
        pkgs.reserve(100);

        srand(std::time(NULL));

        for (int i = 0; i < 5; i++)
        {
            addNode(v2f(150 + i * 100, 300));
        }

        for (int i = 0; i < 4; i++)
            link(nodes_v[i], nodes_v[i + 1], true);
    }

    /*!
     * \brief pkgCount
     * Возвращает количество пакетов, передающихся по сети в данный момент времени
     */
    inline unsigned pkgCount()
    {
        return pkgs.size();
    }

    /*!
     * \brief getTime
     * Получить текущее время моделирования
     * \return
     */
    inline float getTime() const {
        return time;
    }

    /*!
     * \brief getTimePtr
     * Получить указатель на текущее время моделирования
     * \return
     */
    inline float* getTimePtr() {
        return &time;
    }

    /*!
     * \brief package
     * Получить пакет с указанным порядковым номером
     * \param idx Порядковый номер пакета
     * \return Пакет с указанным порядковым номером
     */
    inline NetworkPackage& package(unsigned idx)
    {
        return pkgs[idx];
    }

    /*!
     * \brief addNode
     * Добавить узел в модель
     * \param pos Координаты узла
     */
    inline void addNode(v2f pos)
    {
        unsigned mn = 0;
        for (unsigned i = 0; i < nodes_v.size(); i++)
        {
            if (nodes_v[i]->number() > mn)
                mn = nodes_v[i]->number();
        }

        nodes_v.push_back(new NetworkNode(pos, mn + 1, &time));
    }

    /*!
     * \brief nodes
     * Получить список всех узлов, имеющихся в модели
     * \return Список узлов модели (только для чтения)
     */
    inline const std::vector<NetworkNode*>& nodes()
    {
        return nodes_v;
    }

    /*!
     * \brief getLinks
     * Получить список сетевых подключений модели
     * \return Список сетевых подключений (только для чтения)
     */
    inline const std::vector<NetworkConnection>& getLinks()
    {
        return links;
    }

    /*!
     * \brief nodes_count
     * Получить количество узлов
     */
    inline unsigned nodes_count()
    {
        return nodes_v.size();
    }

    /*!
     * \brief link
     * Добавить соединение между парой узлов
     * \param n1 Первый узел
     * \param n2 Второй узел
     * \param act Начальное состояние соединения (активно / неактивно)
     * \param spd Скорость соединения
     */
    void link(NetworkNode *n1, NetworkNode *n2, bool act=false, unsigned spd=128)
    {
        auto it = std::find(links.begin(), links.end(), NetworkConnection(n1, n2));
        if (it != links.end()) return;
        links.push_back(NetworkConnection(n1, n2, act));
        links[links.size() - 1].setSpeed(spd);
    }

    /*!
     * \brief connections
     * Получить список сетевых подключений модели
     * \return Список сетевых подключений (только для чтения)
     */
    const std::vector<NetworkConnection>& connections()
    {
        return links;
    }

    /*!
     * \brief step
     * Выполнить шаг моделирования
     */
    void step()
    {
        time += 1.0f / fps;

        for (size_t i = 0; i < pkgs.size(); i++)
        {
            pkgs[i].advance((float)bps / (pkgs[i].bytes() * fps));

            if (pkgs[i].done())
            {
                if (!pkgs[i].dropped())
                    onPkgDest(time, pkgs[i].getType(), pkgs[i].src().number(), pkgs[i].dst().number()/*pkgs[i].dptr()*/);
                pkgs.erase(pkgs.begin() + i--);
            }
        }

        for (size_t i = 0; i < nodes_v.size(); i++)
        {
            nodes_v[i]->step(1.0 / fps);
            if (nodes_v[i]->out_data != nullptr)
            {
                sendPkg(nodes_v[i]->number(), nodes_v[i]->out_addr,
                        nodes_v[i]->out_data, nodes_v[i]->out_size);
                nodes_v[i]->out_data = nullptr;
            }
        }
    }

    /*!
     * \brief removeNearestNode
     * Удалить вершину, наиболее близкую к точке (x, y)
     * \param x, y Координаты вершины
     * \param lim_dist Ограничение максимального расстояния
     * \return true, если вершина была удалена
     */
    bool removeNearestNode(unsigned x, unsigned y, unsigned lim_dist=0)
    {
        int idx = findNearestNode(x, y, lim_dist);
        if (idx != -1)
        {
            removeNode(idx);
            return true;
        }
        else return false;
    }

    /*!
     * \brief removeNearestConnection
     * Удалить соединение, наиболее близкое к точке (x, y)
     * \param x, y Координаты точки
     * \param lim_dist Ограничение на максимальное расстояние
     * \return true, если соединение было удалено
     */
    bool removeNearestConnection(unsigned x, unsigned y, unsigned lim_dist=0)
    {
        int idx = findNearestConnection(x, y, lim_dist);
        if (idx != - 1)
        {   
            for (unsigned i = 0; i < pkgs.size(); i++)
            {
                if (pkgs[i].nc() == &links[idx]) {

                    pkgs.erase(pkgs.begin() + i--);
                    continue; }
            }

            links.erase(links.begin() + idx);

            return true;
        }

        return false;
    }

    /*!
     * \brief removeNode
     * Удалить узел, с порядковым номером idx
     * \param idx Порядковый номер удаляемого узла
     */
    inline void removeNode(unsigned idx)
    {
        auto n = nodes_v[idx];

        for (unsigned i = 0; i < links.size(); i++)
        {
            if (links[i].fst() == n || links[i].snd() == n)
            {
                for (unsigned j = 0; j < pkgs.size(); j++)
                {
                    if (&pkgs[j].src() == nodes_v[idx] || &pkgs[j].dst() == nodes_v[idx])
                    {

                        pkgs.erase(pkgs.begin() + j);
                    }
                }
                links.erase(links.begin() + i);
                i--;
            }
        }

        nodes_v.erase(nodes_v.begin() + idx);
    }

    /*!
     * \brief moveNode
     * Переместить узел в пространстве
     * \param idx Порядковый номер перемещаемого узла
     * \param x, y Точка, куда выполняется перемещение
     */
    void moveNode(unsigned idx, unsigned x, unsigned y)
    {
        nodes_v[idx]->position({(float)x, (float)y});
    }

    /*!
     * \brief findNearestConnection
     * Найти порядковый номер соединения, наиболее близкого к точке (x, y)
     * \param x, y Координаты точки
     * \param lim_dist Ограничение на максимальное расстояние для поиска
     * \return Порядковый номер найденного соединения. -1, если соединение не было найдено
     */
    int findNearestConnection(unsigned x, unsigned y, unsigned lim_dist=0)
    {
        if (links.size() == 0) return -1;

        float dist = std::numeric_limits<float>::max();
        int idx = -1;

        for (unsigned i = 0; i < links.size(); i++)
        {
            float nd = pDist(x, y,
                                 links[i].fst()->position().x,
                                 links[i].fst()->position().y,
                                 links[i].snd()->position().x,
                                 links[i].snd()->position().y);
            if (nd < dist)
            {
                dist = nd;
                idx = i;
            }

        }

        if (lim_dist == 0 || lim_dist > dist)
            return idx;
        return -1;
    }

    /*!
     * \brief findNearestNode
     * Найти узел, наиболее близко расположенный к точке (x, y)
     * \param x, y Координаты точки
     * \param lim_dist Ограничение на максимальное расстояние
     * \return Порядковый номер найденного узла. -1, если узел не был найден
     */
    int findNearestNode(unsigned x, unsigned y, unsigned lim_dist=0)
    {
        if (nodes_v.size() == 0) return -1;
        float dist = nodes_v[0]->position().distanceTo({(float)x, (float)y});
        if (nodes_v.size() == 1 && (lim_dist == 0 || dist < lim_dist)) return 0;
        unsigned idx = 0;
        float t;

        for (unsigned i = 1; i < nodes_v.size(); i++)
        {
            t = nodes_v[i]->position().distanceTo({(float)x, (float)y});
            if (t < dist)
            {
                dist = t;
                idx = i;
            }
        }

        if (lim_dist == 0 || lim_dist > dist)
            return idx;
        return -1;
    }

    /*!
     * \brief dropout
     * Смоделировать выпадание узла из сети. Если drop = false, вернуть узел в прежнее состояние
     * \param x, y Координаты точки, около которой будет выполняться поиск узла
     * \param lim_dist Ограничение на максимальное расстояние
     * \param drop Если true, то выполнится выпадание узла. Если false - узел вернется в прежнее состояние
     */
    void dropout(unsigned x, unsigned y, unsigned lim_dist=0, bool drop=true)
    {
        int idx = findNearestNode(x, y, lim_dist);
        if (idx == -1) return;
        nodes_v[idx]->free();
        for (unsigned i = 0; i < links.size(); i++)
        {
            if (links[i].fst() == nodes_v[idx] || links[i].snd() == nodes_v[idx])
            {
                if (drop) {
                    links[i].deactivate();
                    links[i].fst()->free();
                    links[i].snd()->free();
                    links[i].free();
                    for (unsigned j = 0; j < pkgs.size(); j++)
                    {
                        if (pkgs[j].nc() == &links[i]) {
                            pkgs.erase(pkgs.begin() + j--);
                            continue; }
                    }
                }
                else links[i].activate();
            }
        }
    }

    /*!
     * \brief activateNearestConnection
     * Активировать / отключить соединение, наиболее близко расположенное к точке (x, y)
     * \param x, y Координаты точки
     * \param lim_dist Ограничение на максимальное расстояние для поиска
     * \param act Если true, то соединение будет активировано. Если false, соединение будет отключено.
     */
    void activateNearestConnection(unsigned x, unsigned y, unsigned lim_dist=0, bool act=true)
    {
        int idx = findNearestConnection(x, y, lim_dist);
        if (idx != -1)
        {
            for (unsigned i = 0; i < pkgs.size(); i++)
                if (pkgs[i].nc() == &links[idx])
                {
                    pkgs[i].drop();
                }

            if (act)
            {
                links[idx].activate();
                links[idx].free(false);
            }
            else
            {
                links[idx].free(false);
                links[idx].deactivate();
            }
        }
    }


    /*!
     * \brief sendPkg
     * Моделирование пересылки пакетов между двумя узлами
     * \param from Узел-источник (порядковый номер)
     * \param to Узел-получатель (порядковый номер)
     * \param dt Передаваемые данные
     * \param sz Размер передаваемых данных в байтах
     */
    void sendPkg(unsigned from, unsigned to, void *dt, unsigned sz)
    {
        bool dropped = false;

        NetworkNode *f = nullptr, *t = nullptr;
        for (unsigned i = 0; i < nodes_v.size(); i++)
        {
            if (nodes_v[i]->number() == from) f = nodes_v[i];
            if (nodes_v[i]->number() == to) t = nodes_v[i];
        }

        if (f == nullptr || t == nullptr || !f->canSend() || !t->canReceive()) dropped = true;

        NetworkConnection *cn = nullptr;
        NetworkConnection ct(f, t);

        for (unsigned i = 0; i < links.size(); i++)
        {
            if (links[i] == ct) cn = &links[i];
        }

        if (cn == nullptr || !cn->isActive()) dropped = true;

        pkgs.push_back(NetworkPackage(cn, f, t, dt, sz));
        if (dropped) pkgs[pkgs.size() - 1].drop();
        if (f != nullptr) {  f->setSending(true); }
        if (t != nullptr && !dropped) { t->setReceiving(true); }
        if (cn != nullptr && !dropped) cn->take();
    }

    /*!
     * \brief addRandomNode
     * Добавить случайную вершину в указанную прямоугольную область
     * \param x, y Левая верхняя граница прямоугольной области
     * \param w, h Ширина и высота прямоугольной обрасти
     */
    void addRandomNode(unsigned x, unsigned y, unsigned w, unsigned h)
    {
        addNode(v2f(50 + x + rand() % (w - 50), 50 + y + rand() % (h - 50)));
    }
    /*!
     * \brief fully_connected
     * При chance = 1.0f - сформировать полносвязный граф сети. Иначе, соединение между узлами добавляется с указанной вероятностью
     * \param chance Вероятность, что подключение будет добавлено
     */
    void fully_connected(float chance = 1.0f)
    {
        for (size_t i = 0; i < nodes_v.size(); i++)
        {
            for (size_t j = 0; j < nodes_v.size(); j++)
            {
                if (j >= i) continue;
                if (rand() % 1000 < chance * 1000.0f)
                link(nodes_v[i], nodes_v[j], true);
            }
        }
    }

    /*!
     * \brief clear
     * Полная очистка модели. Удаление всех узлов, пакетов и соединений
     */
    void clear()
    {
        nodes_v.clear();
        links.clear();
/*
        for (size_t i = 0; i < pkgs.size(); i++)
            onPkgDest(pkgs[i].dptr());*/

        pkgs.clear();
    }

    /*!
     * \brief setCenter
     * Задать главный узел
     * \param x, y Координаты точки, от которой будет выполняться поиск узла
     */
    void setCenter(unsigned x, unsigned y)
    {
        for (size_t i = 0; i < nodes_v.size(); i++)
            nodes_v[i]->resetCentral();
        int idx = findNearestNode(x, y, 12);
        center = idx;
        if (idx == -1) return;
        nodes_v[idx]->setCentral();
    }
    /*!
     * \brief setSource
     * Задать узел-источник данных
     * \param x, y Координаты точки, от которой будет выполняться поиск узла
     */
    void setSource(unsigned x, unsigned y)
    {
        for (size_t i = 0; i < nodes_v.size(); i++)
            nodes_v[i]->setSource(false);
        int idx = findNearestNode(x, y, 12);
        source = idx;
        if (idx == -1) return;
        nodes_v[idx]->setSource();
        for (size_t i = 0; i < nodes_v.size(); i++)
            nodes_v[i]->getAgent().source = nodes_v[idx]->number();
    }
    /*!
     * \brief setDest
     * Задать узел-получатель данных
     * \param x, y Координаты точки, от которой будет выполняться поиск узла
     */
    void setDest(unsigned x, unsigned y)
    {
        for (size_t i = 0; i < nodes_v.size(); i++)
            nodes_v[i]->setDest(false);
        int idx = findNearestNode(x, y, 12);
        dest = idx;
        if (idx == -1) return;
        nodes_v[idx]->setDest();
        for (size_t i = 0; i < nodes_v.size(); i++)
            nodes_v[i]->getAgent().dest = nodes_v[idx]->number();
    }

    int getSource() const {
        return source;
    }

    int getDest() const {
        return dest;
    }

    int getCenter() const {
        return center;
    }

    byte getSourceAddr() const {
        if (source >= 0 && source <= (int)nodes_v.size())
            return (byte)(nodes_v[source]->number());
        else return NO_NODE;
    }

    byte getDestAddr() const {
        if (dest >= 0 && dest <= (int)nodes_v.size())
            return (byte)(nodes_v[dest]->number());
        else return NO_NODE;
    }

    byte getCenterAddr() const {
        if (center >= 0 && center <= (int)nodes_v.size())
            return (byte)(nodes_v[center]->number());
        else return NO_NODE;
    }

    byte getAddr(int idx) const {
        if (idx >= 0 && idx <= (int)nodes_v.size())
            return nodes_v[idx]->number();
        else return NO_NODE;
    }
};


