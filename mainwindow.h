#pragma once
#include <QMainWindow>
#include <QObject>
#include <QTimer>
#include "network_model.h"
#include "network_view.h"
#include "network_detailed_view.h"
#include "logger.h"
#include <QMouseEvent>
#include <QElapsedTimer>
#include <deque>

namespace Ui {
class MainWindow;
}

/*!
 * \brief MainWindow
 * Класс окна для отображения графического интерфейса пользователя
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     * \brief MainWindow
     * Определяет параметризованный конструктор формы
     * \param parent Ссылка на родительское окно (при наличии)
     */
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    /*!
     * \brief suspend_rendering
     * Приостановка отрисовки графики (используется для экономии ресурсов при длительном моделировании)
     */
    bool suspend_rendering = false;
    /*!
     * \brief cpu_graph
     * Дек, хранящий измерения загруженности процессора для последующего отображения на форме
     */
    std::deque<float> cpu_graph;
    /*!
     * \brief ui
     * Ссылка на экземпляр класса MainWindow
     */
    Ui::MainWindow *ui;
    /*!
     * \brief animate
     * При установке данного значения в false, моделирование приостанавливается
     */
    bool animate;
    /*!
     * \brief timerId
     * Идентификатор таймера, выполняющего перерисовку окна с определенным интервалом.
     * Данный таймер реализует анимацию модели.
     */
    int timerId;
    /*!
     * \brief avg_load
     * Средняя загрузка процессора за последние 10 измерений
     */
    float avg_load = 0;
    /*!
     * \brief nodeHovered
     * Указывает на номер узла, на который в данный момент наведен указатель
     */
    int nodeHovered = -1;
    /*!
     * \brief linkHovered
     * Указывает на порядковый номер соединения, на которое в данный момент наведен указатель мыши
     */
    int linkHovered = -1;/*!
     * \brief packageHovered
     * Указывает на порядковый номер пакета, на который в данный момент наведен указатель
     */
    int packageHovered = -1;

protected:
    /*!
     * \brief model
     * Модель сети
     */
    NetworkModel model;
    /*!
     * \brief view
     */
    NetworkView view;
    /*!
     * \brief details
     * Представление модели
     */
    NetworkDetailedView details;
    /*!
     * \brief logger
     * Логгер отправленных пакетов
     */
    Logger logger;

    /*!
     * \brief paintEvent
     * Событие перерисовки окна
     */
    void paintEvent(QPaintEvent*);
    /*!
     * \brief mouseDoubleClickEvent
     * Событие двойного клика данного окна
     * \param me
     */
    void mouseDoubleClickEvent(QMouseEvent* me);
    /*!
     * \brief mouseMoveEvent
     * Событие перемещения мыши в пределах окна
     * \param me Параметны события - координаты мыши и нажатые клавиши
     */
    void mouseMoveEvent(QMouseEvent *me);
    /*!
     * \brief mousePressEvent
     * Событие нажатия клавиши мыши
     * \param me Параметны события - координаты мыши и нажатые клавиши
     */
    void mousePressEvent(QMouseEvent *me);
    /*!
     * \brief mouseReleaseEvent
     * Событие отпускания кнопки мыши
     * \param me Параметны события - координаты мыши и нажатые клавиши
     */
    void mouseReleaseEvent(QMouseEvent *me);
    /*!
     * \brief timerEvent
     * Событие, вызываемое таймером с некоторым интервалом времени
     */
    void timerEvent(QTimerEvent *);
    /*!
     * \brief wheelEvent
     * Событие прокручивания колеса мыши
     */
    void wheelEvent(QWheelEvent *);
    /*!
     * \brief keyPressEvent
     * Событие нажатия кнопки на клавиатуре
     */
    void keyPressEvent(QKeyEvent *);

private slots:
    /*!
     * \brief on_buttonPlay_clicked
     * Событие нажатия кнопки "Play"
     * для запуска / приостановки процесса моделирования
     */
    void on_buttonPlay_clicked();
    /*!
     * \brief on_buttonStep_clicked
     * Событие нажатия кнопки "Step"
     * для воспроизведения моделирования по шагам
     */
    void on_buttonStep_clicked();
    /*!
     * \brief on_horizontalSlider_valueChanged
     * Событие перемещения горизонтального слайдера
     * для регулировки скорости работы модели
     * \param value Значение регулировки
     */
    void on_horizontalSlider_valueChanged(int value);
    void on_pushButton_2_clicked();
    void on_pushButton_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();
    void on_lineEdit_textChanged(const QString &arg1);
    void on_lineEdit_returnPressed();
    void on_lineEdit_2_returnPressed();
    void on_lineEdit_3_returnPressed();
    void on_lineEdit_4_returnPressed();
    void on_lineEdit_5_returnPressed();
    void on_lineEdit_6_returnPressed();
    void on_lineEdit_7_returnPressed();
    void on_lineEdit_8_returnPressed();
    void on_lineEdit_9_returnPressed();
    void on_lineEdit_10_returnPressed();
    void on_lineEdit_11_returnPressed();
};
