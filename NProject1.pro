#-------------------------------------------------
#
# Project created by QtCreator 2018-07-23T00:37:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NProject1
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += main.cpp\
        mainwindow.cpp \
    v2f.cpp \
    utility.cpp \
    network_agent.cpp \
    params.cpp

HEADERS  += mainwindow.h \
    v2f.h \
    network_node.h \
    network_model.h \
    network_agent.h \
    network_view.h \
    v2i.h \
    utility.h \
    network_connection.h \
    network_package.h \
    cpu_usage.h \
    network_detailed_view.h \
    timerswitch.h \
    timeouts.h \
    types.h \
    logger.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -std=c++11
