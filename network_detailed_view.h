#pragma once
#include <QPaintDevice>
#include <QPainter>
#include <QWidget>
#include "network_model.h"

// установ
enum CurrentView { None, Node, Connection, Package };

/*!
 * \brief NetworkDetailedView
 * Отображает детальную информацию о компонентах сети
 */
class NetworkDetailedView {
    /*!
     * \brief model
     * Модель, для которой отображаются дополнительные сведения
     */
    NetworkModel *model;
    /*!
     * \brief state
     * Тип элемента, о котором в данный момент отображаются сведения
     */
    CurrentView state = CurrentView::None;
    /*!
     * \brief curNode
     * Узел, выбранный пользователем для отображения дополнительной информации о нем
     */
    int curNode = -1;
    /*!
     * \brief curConn
     * Соединение, выбранное пользователем для отображения дополнительной информации о нем
     */
    int curConn = -1;
    /*!
     * \brief curPkg
     * Пакет, выбранный пользователем для отображения дополнительной информации о нем
     */
    int curPkg = -1;
public:
    /*!
     * \brief NetworkDetailedView
     * Конструктор по умолчанию. Отображаемая модель изначально не задана
     */
    NetworkDetailedView(): model(nullptr) { }
    /*!
     * \brief NetworkDetailedView
     * Параметризованный конструктор. Позволяет указать, для какой модели требуется отображение дополнительных сведений
     * \param m Модель, для которой требуется отображение дополнительных сведений
     */
    NetworkDetailedView(NetworkModel *m): model(m) { }
    /*!
     * \brief view
     * Метод отображения детальных сведений о компоненте сети
     * \param d
     * Контекст отображения
     * \param w
     * Область отображения
     */
    void view(QPaintDevice *d, QWidget *w) {
        if (model == nullptr) return;
        QPainter q(d);

        q.translate(w->x(), w->y());
        q.setClipRect(0, 0, w->width(), w->height());
        switch (state) {
        case CurrentView::None:
        {
            q.drawText(0, 0, q.clipBoundingRect().width(),
                       q.clipBoundingRect().height(), Qt::AlignLeft,
                       "Time: " + QString::number(model->getTime(), 's', 3));
            break;
        }
        case CurrentView::Node:
        {
            NetworkNode *n = model->nodes()[curNode];
            NetworkAgent *a = &(n->getAgent());

            QString info = "x: " + QString::number(model->nodes()[curNode]->position().x);
            info += ", y: " + QString::number(model->nodes()[curNode]->position().y);
            info += "\nvx: " + QString::number(model->nodes()[curNode]->speed().x);
            info += ", vy: " + QString::number(model->nodes()[curNode]->speed().y);
            info += "\n" + QString(model->nodes()[curNode]->canSend() ? "can send" : " ");
            info += "\n" + QString(model->nodes()[curNode]->canReceive() ? "can receive" : " ");
            info += "\n\nNeighbors: \n";
            for (size_t i = 0; i < a->listNeighbor.size(); i++)
                info += QString::number(a->listNeighbor[i]) + " ";

            info += "\n\n2-Step Routes: \n";
            for (size_t i = 0; i < a->listNeighbor2.size(); i++)
                info += "(" + QString::number(a->listNeighbor2[i].neighbor) + ", " +
                         QString::number(a->listNeighbor2[i].neighbor2) + ") ";

            info += "\n\nRoutes List: \n";
            for (size_t i = 0; i < a->listRoutes.size(); i++) {
                for (size_t j = 0; j < a->listRoutes[i].size(); j++) {
                    info += QString::number(a->listRoutes[i][j]) + " ";
                }

                info += "\n";
            }




            q.drawText(0, 0, q.clipBoundingRect().width(), q.clipBoundingRect().height(), Qt::AlignLeft, info);
            break;
        }
        case CurrentView::Connection:
        {
            q.drawText(0, 0, 32, 32, Qt::AlignHCenter, "Conn");
            break;
        }
        case CurrentView::Package:
        {
            q.drawText(0, 0, 32, 32, Qt::AlignHCenter, "Package");
            break;
        }
        default:
            break;
        }
    }
    /*!
     * \brief reset
     * Сброс отображаемого элемента
     */
    void reset() {
        state = CurrentView::None;
        curNode = curConn = curPkg = -1; }
    /*!
     * \brief node
     * Вывести дополнительные сведения об узле сети
     * \param idx Порядковый номер узла, для которого требуются дополнительные сведения
     */
    void node(int idx) {
        state = CurrentView::Node;
        curNode = idx; }
    /*!
     * \brief connection
     * Вывести дополнительные сведения о соединении
     * \param idx Порядковый номер соединения, для которого требуются дополнительные сведения
     */
    void connection(int idx) {
        state = CurrentView::Connection;
        curConn = idx; }
    /*!
     * \brief package
     * Вывести дополнительные сведения о передаваемом пакете
     * \param idx Порядковый номер пакета, для которого требуются дополнительные сведения
     */
    void package(int idx) {
        state = CurrentView::Package;
        curPkg = idx; }
    /*!
     * \brief setModel
     * Назначить модель, для которой будет производиться вывод дополнительных сведений
     * \param m Указатель на объект модели
     */
    void setModel(NetworkModel *m) {
        model = m; }
};
