#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "utility.h"
#include <QMessageBox>
#include <QGraphicsScene>
#include "cpu_usage.h"
#include <QPainter>
#include <cmath>
#include <fstream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    view.setModel(&model);
    details.setModel(&model);

    model.onPkgSent = [this] (void *) {

    };

    model.onPkgDest = [this] (float time, byte type, byte from, byte to) {
        logger.add(LogEntry(time, type, from, to));
    };

    ui->setupUi(this);
    timerId = startTimer(33);
    animate = true;
    CPU::init();
    for (unsigned i = 0; i < 140; i++)
        cpu_graph.push_back(0.0f);
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent*)
{
    view.view(this, ui->frame->width(), ui->frame->height());
    details.view(this, ui->frame_details);
    QPainter q(this);
    q.translate(ui->frame_2->x(), ui->frame_2->y());
    q.setClipRect(0, 0, 140, 31);
    unsigned ctr = 0;
    for (auto it = cpu_graph.begin(); it != cpu_graph.end(); it++)
    {
        float val = *it;
        q.drawLine(ctr, 31, ctr, 31 - 30 * val);
        ctr++;
    }


}

void MainWindow::mouseDoubleClickEvent(QMouseEvent *me)
{
    view.ondbclick(me->x(), me->y(), me->button());
}

void MainWindow::mouseMoveEvent(QMouseEvent *me)
{
    view.onmousemove((unsigned)(me->x()), (unsigned)(me->y()), me->button());
}

void MainWindow::mousePressEvent(QMouseEvent *me)
{
    view.onmousepress(me->x(), me->y(), me->button());
    int nh = view.nodeHovered();
    int lh = view.linkHovered();
    int ph = view.packageHovered();

    if (lh != -1) details.connection(lh);
    if (nh != -1) details.node(nh);
    if (ph != -1) details.package(ph);
    if (lh == -1 && nh == -1 && ph == -1) details.reset();

}

void MainWindow::mouseReleaseEvent(QMouseEvent *me)
{
    view.onmouserelease(me->x(), me->y(), me->button());
}

void MainWindow::timerEvent(QTimerEvent *)
{
    QPoint m = mapFromGlobal(QCursor::pos());
    view.updMouse(m.x(), m.y());

    nodeHovered = view.nodeHovered();
    linkHovered = view.linkHovered();
    packageHovered = view.packageHovered();

    if (!suspend_rendering)
        ui->frame->update();
    ui->frame_2->update();
    ui->frame_details->update();
    if (animate)
    {
        if (ui->checkBox->checkState() == 2)
        {
            if (model.pkgCount() == 0) {
                ui->horizontalSlider->setValue(50);
                ui->horizontalSlider->update();
            }
            else
            {
                ui->horizontalSlider->setValue(30);
                ui->horizontalSlider->update();
            }
        }

        view.step();
    }
    avg_load = (avg_load * 9 + CPU::getCurrentValue() * CPU::num_cores()) / 10.0f;
    cpu_graph.pop_front();
    cpu_graph.push_back(avg_load / 100.0f);

}

void MainWindow::wheelEvent(QWheelEvent *w)
{
    if (w->orientation() == Qt::Vertical)
        view.onmousewheel(w->x(), w->y(), signum(w->delta()));
}

void MainWindow::keyPressEvent(QKeyEvent *k)
{
    if (k->key() == Qt::Key::Key_Space)
        emit on_buttonPlay_clicked();

    if (k->key() == Qt::Key::Key_X)
        suspend_rendering = !suspend_rendering;

    view.kbd(k);

}

void MainWindow::on_buttonPlay_clicked()
{
    if (animate)
    {
        ui->buttonPlay->setText(QString::fromUtf8("▶"));
        animate = !animate;
        ui->buttonStep->setEnabled(true);
    }
    else
    {
        ui->buttonPlay->setText(QString::fromUtf8("◼"));
        ui->buttonStep->setEnabled(false);
        animate = !animate;
    }
}

void MainWindow::on_buttonStep_clicked()
{
    animate = true;
    timerEvent(NULL);
    animate = false;
}

void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    float f = (float)value / 25 - 2.0f;
    view.setSpeed(pow(10.0f, f));
}

void MainWindow::on_pushButton_2_clicked()
{
    QString filename = ui->textEdit->document()->toPlainText();;
    std::ofstream output(filename.toLatin1().data());

    unsigned n = model.nodes_count();
    output << n << std::endl;
    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < n; j++) {
            bool fnd = false;
            for (size_t k = 0; k < model.connections().size(); k++)
            {
                if (model.connections()[k].fst()->number() == i + 1 && model.connections()[k].snd()->number() == j + 1) {
                    output << model.connections()[k].getSpeed() << ' ';
                    fnd = true;
                    break;
                }
            }
            if (!fnd) output << 0 << ' ';
        }

        output << "\n";
    }

    output.close();
}

void MainWindow::on_pushButton_clicked()
{
    QString filename = ui->textEdit->document()->toPlainText();;
    std::ifstream input(filename.toLatin1().data());
    model.clear();
    unsigned n, t;
    input >> n;

    for (size_t i = 0; i < n; i++) model.addRandomNode(50, 50, 400, 400);

    for (size_t i = 0; i < n; i++)
    {
        for (size_t j = 0; j < n; j++) {
            input >> t;
            if (t != 0) {
                model.link(model.nodes()[i], model.nodes()[j], true, t);
            }
        }
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    bool kde = true;
    logger.save("log.txt");
    if (kde)
        system("kwrite log.txt");
}

void MainWindow::on_pushButton_4_clicked()
{
    logger.clear();
}

//void MainWindow::on_lineEdit_textChanged(const QString &arg1)
//{

//}

void MainWindow::on_lineEdit_returnPressed()
{
    float val = ui->lineEdit->text().toFloat();
    for (size_t i = 0; i < model.nodes_count(); i++) {
        model.nodes()[i]->getAgent().TTL = (byte)val;
    }
}

void MainWindow::on_lineEdit_2_returnPressed()
{
    float val = ui->lineEdit_2->text().toFloat();
    for (size_t i = 0; i < model.nodes_count(); i++) {
        model.nodes()[i]->getAgent().HND_TIME = val;
    }
}

void MainWindow::on_lineEdit_3_returnPressed()
{
    float val = ui->lineEdit_3->text().toFloat();
    for (size_t i = 0; i < model.nodes_count(); i++) {
        model.nodes()[i]->getAgent().HND_ANSWER_TIME = val;
    }
}

void MainWindow::on_lineEdit_4_returnPressed()
{
    float val = ui->lineEdit_4->text().toFloat();
    for (size_t i = 0; i < model.nodes_count(); i++) {
        model.nodes()[i]->getAgent().HELLO_TIME = val;
    }
}

void MainWindow::on_lineEdit_5_returnPressed()
{
    float val = ui->lineEdit_5->text().toFloat();
    for (size_t i = 0; i < model.nodes_count(); i++) {
        model.nodes()[i]->getAgent().HELLO_OK_TIME = val;
    }
}

void MainWindow::on_lineEdit_6_returnPressed()
{
    float val = ui->lineEdit_6->text().toFloat();
    for (size_t i = 0; i < model.nodes_count(); i++) {
        model.nodes()[i]->getAgent().DATA_ANSWER_TIME = val;
    }
}

void MainWindow::on_lineEdit_7_returnPressed()
{
    float val = ui->lineEdit_7->text().toFloat();
    for (size_t i = 0; i < model.nodes_count(); i++) {
        model.nodes()[i]->getAgent().REPEATED_DQUERY_TIME = val;
    }
}

void MainWindow::on_lineEdit_8_returnPressed()
{
    float val = ui->lineEdit_8->text().toFloat();
    for (size_t i = 0; i < model.nodes_count(); i++) {
        model.nodes()[i]->getAgent().DATA_TRANSFERRED_TIME = val;
    }
}

void MainWindow::on_lineEdit_9_returnPressed()
{
    float val = ui->lineEdit_9->text().toFloat();
    for (size_t i = 0; i < model.nodes_count(); i++) {
        model.nodes()[i]->getAgent().DATA_REPEATED_TIME = val;
    }
}

void MainWindow::on_lineEdit_10_returnPressed()
{
    float val = ui->lineEdit_10->text().toFloat();
    for (size_t i = 0; i < model.nodes_count(); i++) {
        model.nodes()[i]->getAgent().ROUTE_SEARCH_TIME = val;
    }
}

void MainWindow::on_lineEdit_11_returnPressed()
{
    float val = ui->lineEdit_11->text().toFloat();
    for (size_t i = 0; i < model.nodes_count(); i++) {
        model.nodes()[i]->getAgent().ACTUAL_ROUTE_TIME = val;
    }
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    // pass
}
