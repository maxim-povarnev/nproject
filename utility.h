#pragma once
#include "v2i.h"
#include "v2f.h"

/*!
 * \brief pDist - расстояние от точки до прямой
 * \param x, y - координаты точки
 * \param x1, y1 - координаты первой точки, через которую проходит прямая
 * \param x2, y2 - координаты второй точки, через которую проходит прямая
 * \return Расстояние от точки (x, y) до прямой, проходящей через точки
 *  (x1, y1) и (x2, y2)
 */
float pDist(float x, float y, float x1, float y1, float x2, float y2);

/*!
 * \brief signum - функция сигнум
 * \param x - входной параметр
 * \return -1 если x < 0, 0 если x == 0, 1 если x > 0
 */
int signum(int x);

/*!
 * \brief lerp - линейная интерполяция точки (v2i)
 * \param a - исходная точка
 * \param b - точка, в которую осуществляется интерполяция
 * \param factor - степень интерполяции (от 0 до 1)
 * \return вектор, смещенный из точки a в точку b на коэффициент factor
 */
v2i lerp(const v2i &a, const v2i &b, float factor);

/*!
 * \brief lerp - линейная интерполяция точки (v2f)
 * \param a - исходная точка
 * \param b - точка, в которую осуществляется интерполяция
 * \param factor - степень интерполяции (от 0 до 1)
 * \return вектор, смещенный из точки a в точку b на коэффициент factor
 */
v2f lerp(const v2f &a, const v2f &b, float factor);
