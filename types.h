#pragma once
#include <vector>
typedef unsigned long size_t;
typedef unsigned char byte; // краткий псевдоним для unsigned char
typedef unsigned long long uint64; // краткий псевдоним для unsigned long long
typedef std::vector<byte> route; // краткий псевдоним для std::vector<byte>

/*!
 * \brief gb
 * Извлечение байта
 * \param data Указатель на данные
 * \param sh Порядковый номер извлекаемого байта
 * \return Ссылка на извлекаемый байт
 */
inline byte& gb(void *data, size_t sh) {
    return ((byte*)data)[sh];
}

/*!
 * \brief PKG_ACCESS_QUERY
 * Значение первого байта в пакете типа ACCESS_QUERY
 */
const byte PKG_ACCESS_QUERY = 0;
/*!
 * \brief PKG_ACCESS_ANSWER
 * Значение первого байта в пакете типа ACCESS_ANSWER
 */
const byte PKG_ACCESS_ANSWER = 1;
/*!
 * \brief PKG_HELLO_QUERY
 * Значение первого байта в пакете типа HELLO_QUERY
 */
const byte PKG_HELLO_QUERY = 2;
/*!
 * \brief PKG_HELLO_ANSWER
 * Значение первого байта в пакете типа HELLO_ANSWER
 */
const byte PKG_HELLO_ANSWER = 3;
/*!
 * \brief PKG_HELLO_ERROR
 * Значение первого байта в пакете типа HELLO_ERROR
 */
const byte PKG_HELLO_ERROR = 4;
/*!
 * \brief PKG_ROUTE_QUERY
 * Значение первого байта в пакете типа ROUTE_QUERY
 */
const byte PKG_ROUTE_QUERY = 5;
/*!
 * \brief PKG_ROUTE_ANSWER
 * Значение первого байта в пакете типа ROUTE_ANSWER
 */
const byte PKG_ROUTE_ANSWER = 6;
/*!
 * \brief PKG_ROUTE_ERROR
 * Значение первого байта в пакете типа ROUTE_ERROR
 */
const byte PKG_ROUTE_ERROR = 7;
/*!
 * \brief PKG_DATA_QUERY
 * Значение первого байта в пакете типа DATA_QUERY
 */
const byte PKG_DATA_QUERY = 8;
/*!
 * \brief PKG_DATA_ANSWER
 * Значение первого байта в пакете типа DATA_ANSWER
 */
const byte PKG_DATA_ANSWER = 9;
/*!
 * \brief PKG_DATA_ERROR
 * Значение первого байта в пакете типа DATA_ERROR
 */
const byte PKG_DATA_ERROR = 10;
/*!
 * \brief PKG_STATUS_MESSAGE
 * Значение первого байта в пакете типа STATUS_MESSAGE
 */
const byte PKG_STATUS_MESSAGE = 11;
/*!
 * \brief PKG_COMMAND_MESSAGE
 * Значение первого байта в пакете типа COMMAND_MESSAGE
 */
const byte PKG_COMMAND_MESSAGE = 12;
/*!
 * \brief PKG_APPLICATION_MESSAGE
 * Значение первого байта в пакете типа APPLICATION_MESSAGE
 */
const byte PKG_APPLICATION_MESSAGE = 13;

