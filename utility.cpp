#include "utility.h"
#include <cmath>

float pDist(float x, float y, float x1, float y1, float x2, float y2) {
  float A = x - x1;
  float B = y - y1;
  float C = x2 - x1;
  float D = y2 - y1;

  float dot = A * C + B * D;
  float len_sq = C * C + D * D;
  float param = -1;
  if (len_sq != 0)
      param = dot / len_sq;

  float xx, yy;

  if (param < 0) {
    xx = x1;
    yy = y1;
  }
  else if (param > 1) {
    xx = x2;
    yy = y2;
  }
  else {
    xx = x1 + param * C;
    yy = y1 + param * D;
  }

  float dx = x - xx;
  float dy = y - yy;
  return sqrt(dx * dx + dy * dy);
}

int signum(int x)
{
    return (0 < x) - (x < 0);
}

v2i lerp(const v2i &a, const v2i &b, float f)
{
    v2i delta = b - a;
    return a + delta * f;
}

v2f lerp(const v2f &a, const v2f &b, float f)
{
    v2f delta = b - a;
    return a + delta * f;
}
