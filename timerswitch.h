#pragma once

/*!
 * \brief TimerSwitch
 * Данный класс обеспечивает функционал для отслеживания
 * времени наступления периодических событий.
 */
class TimerSwitch {

    /*!
     * \brief interval
     * Интервал появления события
     */
    float interval;
    /*!
     * \brief timestamp
     * Текущее время моделирования
     */
    float timestamp;
    /*!
     * \brief last_timestamp
     * Время последней активации
     */
    float last_timestamp;
    /*!
     * \brief state
     * Флаг появления события
     */
    bool state;

public:

    /*!
     * \brief TimerSwitch
     * Параметризованный конструктор класса
     * \param ts Начальный момент появления объекта (0 по умолчанию)
     * \param _interval Интервал появления события
     */
    TimerSwitch(float _interval = 5.0f)
    {
        interval = _interval;
        state = false;
        timestamp = 0.0f;
        last_timestamp = 0.0f;
    }

    /*!
     * \brief step
     * Выполнить шаг моделирования
     * \param ts Интервал времени моделирования
     */
    void step(float ts)
    {
        timestamp += ts;
        if (timestamp - last_timestamp > interval)
        {
            last_timestamp = timestamp;
            state = true;
        }
    }

    /*!
     * \brief isActive
     * Позволяет определить возникновение события
     * \return Флаг появления события
     */
    inline bool isActive() const {
        return state;
    }

    /*!
     * \brief activate
     * Установить флаг появления события
     */
    inline void activate() {
        state = true;
    }

    inline void setInterval(float interv_) {
        interval = interv_;
    }

    /*!
     * \brief turnOff
     * Сбросить флаг появления события
     */
    inline void turnOff() {
        state = false;
    }
};
