#pragma once

#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#ifdef __linux__
#include "sys/times.h"
#include "sys/vtimes.h"
#endif

namespace CPU {

#ifdef __linux__

    static clock_t lastCPU, lastSysCPU, lastUserCPU;
    static int numProcessors;

    /*!
     * \brief init
     * Инициализация счетчиков производительности
     */
    void init(){
        FILE* file;
        struct tms timeSample;
        char line[128];

        lastCPU = times(&timeSample);
        lastSysCPU = timeSample.tms_stime;
        lastUserCPU = timeSample.tms_utime;

        file = fopen("/proc/cpuinfo", "r");
        numProcessors = 0;
        while(fgets(line, 128, file) != NULL){
            if (strncmp(line, "processor", 9) == 0) numProcessors++;
        }
        fclose(file);
    }

    /*!
     * \brief getCurrentValue
     * Получить загруженность CPU
     * \return Степень загруженности процессора в процентах
     */
    double getCurrentValue(){
        struct tms timeSample;
        clock_t now;
        double percent;

        now = times(&timeSample);
        if (now <= lastCPU || timeSample.tms_stime < lastSysCPU ||
            timeSample.tms_utime < lastUserCPU){
            percent = -1.0;
        }
        else{
            percent = (timeSample.tms_stime - lastSysCPU) +
                (timeSample.tms_utime - lastUserCPU);
            percent /= (now - lastCPU);
            percent /= numProcessors;
            percent *= 100;
        }
        lastCPU = now;
        lastSysCPU = timeSample.tms_stime;
        lastUserCPU = timeSample.tms_utime;

        return percent;
    }

    /*!
     * \brief num_cores
     * Возвращает количество ядер процессора
     */
    unsigned num_cores()
    {
        return numProcessors;
    }

#endif

#ifdef _WIN32

    void init(){ } // not impl. yet
    double getCurrentValue()
    {
        return 0.0;
    }

    unsigned num_cores()
    {
        return 1;
    }

#endif




}
