#pragma once
#include <QPainter>
#include <QMessageBox>
#include <QKeyEvent>
#include <QTimer>
#include <QCursor>
#include <utility>
#include "network_model.h"
#include "network_connection.h"
#include "v2i.h"

/*!
 * \brief NetworkView
 * Представление модели в графическом интерфейсе пользователя.
 * Позволяет взаимодействовать с моделью в режиме реального времени.
 */
class NetworkView {

    /*!
 * \brief model
 * Модель, для которой выполняется отображение
 */
NetworkModel *model;
/*!
 * \brief timer
 * Объект таймера, выполняющий перерисовку
 */
QTimer *timer;
/*!
 * \brief grab_idx
 * Порядковый номер перемещаемого узла
 */
int grab_idx = -1;
/*!
 * \brief conn_idx
 * Порядковый номер узла, из которого проводится соединение
 */
int conn_idx = -1;
/*!
 * \brief conn_tx, conn_ty
 * Координаты узла, из которого проводится соединение
 */
unsigned conn_tx, conn_ty;
/*!
 * \brief time
 * Время моделирования
 */
float time = 0.0f;
/*!
 * \brief speed
 * Скорость моделирования
 */
float speed = 1.0f;
/*!
 * \brief mouse
 * Координаты мыши
 */
v2i mouse;

public:

    /*!
     * \brief NetworkView
     * Конструктор по умолчанию. Отображаемая модель изначально не задана.
     */
    NetworkView(): model(nullptr), timer(nullptr) { }

    /*!
     * \brief NetworkView
     * Параметризованный конструктор. Позволяет указать отображаемую модель.
     * \param m Отображаемая модель
     */
    NetworkView(NetworkModel *m)
    {
        model = m;
        timer = nullptr;
    }

    /*!
     * \brief setModel
     * Указать модель, для которой будет выполняться отображение
     * \param m Указатель на объект модели
     */
    void setModel(NetworkModel *m)
    {
        model = m;
    }

    /*!
     * \brief setSpeed
     * Установить скорость моделирования
     * \param s
     */
    void setSpeed(float s)
    {
        speed = s;
    }

    /*!
     * \brief view
     * Метод, выполняющий отображение модели
     * \param w Контекст отображения
     * \param wdt Ширина области отображения
     * \param hgt Высота области отображения
     */
    void view(QPaintDevice *w, int wdt, int hgt)
    {
        QPainter q(w);
        q.setClipRect(0, 0, wdt, hgt);
        q.setRenderHint(QPainter::Antialiasing);

        if (conn_idx != -1)
        {
            q.drawLine(model->nodes()[conn_idx]->position().x + 12,
                       model->nodes()[conn_idx]->position().y + 12, conn_tx, conn_ty);
        }

        for (unsigned i = 0; i < model->getLinks().size(); i++)
        {
            q.setOpacity(0.3 + model->getLinks()[i].isActive() * 0.7); // 0.3 if inactive
            if (model->getLinks()[i].isBusy())
                q.setPen(Qt::red);
            else q.setPen(Qt::black);
            auto n1 = model->getLinks()[i].fst();
            auto n2 = model->getLinks()[i].snd();
            q.drawLine(n1->position().x + 12, n1->position().y + 12, n2->position().x + 12, n2->position().y + 12);
        }

        q.setPen(Qt::black);

        q.setOpacity(1);

        for (auto it = model->nodes().begin(); it != model->nodes().end(); it++)
        {
            if ((*it)->isCentral())
                q.setBrush(QColor(0, 255, 127));
            else if ((*it)->isSource())
                q.setBrush(QColor(255, 128, 32));
            else if ((*it)->isDest())
                q.setBrush(QColor(255, 92, 64));
            else q.setBrush(Qt::yellow);

            if (!(*it)->canSend() || !(*it)->canReceive())
                q.setPen(Qt::red);
            else q.setPen(Qt::black);
            q.drawEllipse((*it)->position().x, (*it)->position().y, 24, 24);
            q.setPen(Qt::black);
            q.drawText((*it)->position().x, (*it)->position().y + 4, 24, 24, Qt::AlignHCenter, QString::number((*it)->number()));
            q.drawText((*it)->position().x + 15, (*it)->position().y + 19, 24, 24, Qt::AlignHCenter, "(" + QString::number((*it)->getAgent().listNeighbor.size())
                       + ";" + QString::number((*it)->getAgent().listNeighbor2.size()) + ")");

        }

        q.setBrush(Qt::green);

        for (unsigned i = 0; i < model->pkgCount(); i++)
        {
            if (model->package(i).dptr() == nullptr)
            {
                q.setPen(Qt::blue);
                q.setOpacity(0.4);
                q.setBrush(Qt::NoBrush);

                switch (model->package(i).getType())
                {
                case PKG_ACCESS_QUERY:
                    q.setPen(Qt::green);
                    break;
                case PKG_ACCESS_ANSWER:
                    q.setPen(Qt::red);
                    break;
                case PKG_HELLO_QUERY:
                    q.setPen(Qt::yellow);
                    break;
                case PKG_HELLO_ANSWER:
                    q.setPen(Qt::blue);
                    break;
                case PKG_HELLO_ERROR:
                    q.setPen(QColor(255, 128, 0));
                default:
                    q.setPen(Qt::black);
                    break;

                }

                if (&model->package(i).src() != nullptr && &model->package(i).dst() == nullptr)
                {
                    int r = (int)(model->package(i).getProgress() * 10) + 5;
                    int r2 = r / 2;
                    q.drawEllipse(model->package(i).src().position().x - r2, model->package(i).src().position().y - r2, 24 + r, 24 + r);

                }

                if (&model->package(i).src() == nullptr || &model->package(i).dst() == nullptr) continue;

                v2i a(model->package(i).src().position().x + 12,
                      model->package(i).src().position().y + 12);
                v2i b(model->package(i).dst().position().x + 12,
                      model->package(i).dst().position().y + 12);

                v2i c = lerp(a, b, model->package(i).getProgress());
                v2i d = lerp(a, b, model->package(i).getProgress() - 0.05f);

                q.setOpacity(0.75);
                q.drawLine(d.x + 2, d.y + 2, c.x + 2, c.y + 2);
                q.setOpacity(1.0);
                continue;
            }

            switch (model->package(i).getType())
            {
            case PKG_ACCESS_QUERY:
                q.setBrush(Qt::green);
                break;
            case PKG_ACCESS_ANSWER:
                q.setBrush(Qt::red);
                break;
            case PKG_HELLO_QUERY:
                q.setBrush(Qt::yellow);
                break;
            case PKG_HELLO_ANSWER:
                q.setBrush(Qt::blue);
                break;
            case PKG_HELLO_ERROR:
                q.setBrush(QColor(255, 128, 0));
                break;
            case PKG_ROUTE_QUERY:
                q.setBrush(QColor(192, 0, 128));
                break;
            case PKG_ROUTE_ANSWER:
                q.setBrush(QColor(128, 0, 128));
                break;
            case PKG_DATA_QUERY:
                q.setBrush(QColor(98, 197, 129));
                break;
            case PKG_DATA_ANSWER:
                q.setBrush(QColor(98, 255, 129));
                break;
            default:
                q.setBrush(Qt::black);
                break;

            }

            q.setOpacity(1.0);
            v2f pos = model->package(i).pos();
            q.setPen(Qt::black);
            q.drawEllipse(pos.x - 4  + 12, pos.y - 4 + 12, 8, 8);
        }

    }

    /*!
     * \brief ondbclick
     * Обработчик события двойного клика мыши
     * \param x Координата X, где осуществлен двойной клик
     * \param y Координата Y, где осуществлен двойной клик
     * \param btn Кнопка мыши, которой осуществлен двойной клик
     */
    void ondbclick(unsigned x, unsigned y, int btn)
    {

        switch (btn)
        {
        case Qt::MouseButton::LeftButton:
            model->addNode({(float)(x - 12), (float)(y - 12)});
            break;

        case Qt::MouseButton::RightButton:
            if (!model->removeNearestNode(x - 12, y - 12, 12))
                model->removeNearestConnection(x - 12, y - 12, 5);
            break;
        }
    }

    /*!
     * \brief onmousemove
     * Обработчик события движения мыши
     * \param x, y Координаты мыши
     * \param btn Нажатая кнопка мыши
     */
    void onmousemove(unsigned x, unsigned y, int btn)
    {
        mouse.x = x;
        mouse.y = y;

        if (grab_idx != -1)
        {
            model->moveNode(grab_idx, x - 12, y - 12);
        }

        if (conn_idx != -1)
        {
            conn_tx = x;
            conn_ty = y;
        }

        switch (btn)
        {
        case Qt::MouseButton::LeftButton:

            break;

        case Qt::MouseButton::RightButton:

            break;
        }
    }

    /*!
     * \brief onmousepress
     * Обработчик события нажатия кнопки мыши
     * \param x, y Координаты мыши
     * \param btn Нажимаемая кнопка
     */
    void onmousepress(unsigned x, unsigned y, int btn)
    {

        switch (btn)
        {
        case Qt::MouseButton::LeftButton:
            grab_idx = model->findNearestNode(x - 12, y - 12, 12);
            break;

        case Qt::MouseButton::RightButton:
            conn_idx = model->findNearestNode(x - 12, y - 12, 12);
            conn_tx = x;
            conn_ty = y;
            break;
        }
    }

    /*!
     * \brief onmouserelease
     * Обработчик события отпускания кнопки мыши
     * \param x, y Координаты мыши
     */
    void onmouserelease(unsigned x, unsigned y, int)
    {
        if (conn_idx != -1)
        {
            int nx = model->findNearestNode(x - 12, y - 12, 12);
            if (nx != -1 && conn_idx != nx)
            {
                model->link(model->nodes()[conn_idx], model->nodes()[nx]);
            }
        }

        grab_idx = -1;
        conn_idx = -1;
    }

    /*!
     * \brief onmousewheel
     * Обработчик события движения колеса прокрутки
     * \param x, y Координаты мыши
     * \param dir Направление прокрутки
     */
    void onmousewheel(unsigned x, unsigned y, int dir)
    {
        dir = (dir + 1) / 2;
        int idx = model->findNearestNode(x - 12, y - 12, 12);
        if (idx == -1)
        model->activateNearestConnection(x - 12, y - 12, 10, dir);
        else
            model->dropout(x - 12, y - 12, 12, !dir);

    }

    /*!
     * \brief kbd
     * Обработчик события нажатия клавиши
     * \param k Нажатая клавиша
     */
    void kbd(QKeyEvent *k)
    {


        if (k->key() == Qt::Key::Key_S)
        {
            step();
        }

        if (k->key() == Qt::Key::Key_R)
        {
            model->addRandomNode(0, 0, 700, 700);
        }

        if (k->key() == Qt::Key::Key_F)
        {
            model->fully_connected(0.33);
        }

        if (k->key() == Qt::Key::Key_C)
        {
            model->clear();
        }

        if (k->key() == Qt::Key::Key_M)
        {
            model->setCenter(mouse.x - 12, mouse.y - 12);
        }

        if (k->key() == Qt::Key::Key_A)
        {
            model->setSource(mouse.x - 12, mouse.y - 12);
        }

        if (k->key() == Qt::Key::Key_B)
        {
            model->setDest(mouse.x - 12, mouse.y - 12);
        }

        if (k->key() == Qt::Key::Key_1)
        {
            int node = model->findNearestNode(mouse.x - 12, mouse.y - 12, 12);
            if (node == -1) return;
            if (model->getDestAddr() == NO_NODE) return;
            NetworkAgent *agent = &(model->nodes()[node]->getAgent());
            agent->enqueue(model->getDestAddr(), mkAccessQuery(model->getAddr(node), model->getDestAddr()), 11);
        }

        if (k->key() == Qt::Key::Key_2)
        {
            int node = model->findNearestNode(mouse.x - 12, mouse.y - 12, 12);
            if (node == -1) return;
            if (model->getDestAddr() == NO_NODE) return;
            NetworkAgent *agent = &(model->nodes()[node]->getAgent());
            agent->enqueue(model->getDestAddr(), mkHelloQuery(model->getAddr(node), model->getDestAddr()), 11);
        }

        if (k->key() == Qt::Key::Key_3)
        {
            if (model->getSourceAddr() == NO_NODE || model->getDestAddr() == NO_NODE) return;
            int idx = model->findNearestNode(mouse.x - 12, mouse.y - 12);
            if (idx == -1) return;
            byte src = model->getSourceAddr();
            int src_idx = model->getSource();
            byte dst = model->getDestAddr();
            byte to = model->getAddr(idx);
            NetworkAgent *agent = &(model->nodes()[src_idx]->getAgent());
            unsigned short rid = (unsigned short)rand();
            route v; v.push_back(src);
            agent->enqueue(to, mkRouteQuery(src, dst, rid, 1, v), 11);
        }

        if (k->key() == Qt::Key::Key_4)
        {

        }

        if (k->key() == Qt::Key::Key_0)
        {
            if (model->getSource() == -1) return;
            NetworkAgent *agent = &(model->nodes()[model->getSource()]->getAgent());
            size_t size = 1000;
            void *data = malloc(size);
            for (size_t i = 0; i < size; i++)
                gb(data, i) = i % 10;
            agent->addTransfer(data, size);
            free(data);

        }


    }

    /*!
     * \brief updMouse
     * Метод, обновляющий данные о расположении мыши на экране
     * \param x, y Координаты мыши
     */
    void updMouse(unsigned x, unsigned y)
    {
        mouse.x = x;
        mouse.y = y;
    }

    /*!
     * \brief step
     * Выполнение шага моделирования
     */
    void step()
    {
        time += speed * 33.0f;
        while ((time -= 1.0f) >= 1.0f)
            model->step();
    }

    /*!
     * \brief nodeHovered
     * Определить порядковый номер узла, на который наведен указатель
     * \return Порядковый номер узла. -1, если указатель не наведен ни на один узел
     */
    int nodeHovered()
    {
        return model->findNearestNode(mouse.x - 12, mouse.y - 12, 12);
    }

    /*!
     * \brief linkHovered
     * Определить порядковый номер соединения, на которое наведен указатель
     * \return Порядковый номер соединения. -1, если указатель не наведен ни на одно соединение.
     */
    int linkHovered()
    {
        return model->findNearestConnection(mouse.x - 12, mouse.y - 12, 5);
    }

    /*!
     * \brief packageHovered
     * Определить порядковый номер пакета, на который наведен указатель мыши.
     * (Метод еще не реализован).
     * \return -1
     */
    int packageHovered()
    {
        return -1;
    }

};
