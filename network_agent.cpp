#include "network_agent.h"

void* mkAccessQuery(byte from, byte to, void *additional, byte add_sz)
{
    size_t pkg_size = 11;
    void *data = malloc(pkg_size);
    memset(data, 0, pkg_size);
    gb(data, 0) = PKG_ACCESS_QUERY;
    gb(data, 1) = from;
    gb(data, 2) = to;
    if (additional != nullptr)
        memcpy((byte*)data + 3, additional, add_sz);
    return data;
}

void* mkAccessAnswer(byte from, byte to, const std::vector<byte> &neighbors, void *additional, byte add_sz)
{
    size_t pkg_size = 12 + neighbors.size();
    void *data = malloc(pkg_size);
    memset(data, 0, pkg_size);
    gb(data, 0) = PKG_ACCESS_ANSWER;
    gb(data, 1) = from;
    gb(data, 2) = to;
    gb(data, 3) = (byte)neighbors.size();
    for (size_t i = 0; i < neighbors.size(); i++)
        gb(data, 4 + i) = neighbors[i];
    if (additional != nullptr)
    {
        memcpy((byte*)data + 4, additional, add_sz);
    }
    return data;
}

void* mkHelloQuery(byte from, byte to, void *additional, byte add_sz)
{
    size_t pkg_size = 11;
    void *data = malloc(pkg_size);
    memset(data, 0, pkg_size);
    gb(data, 0) = PKG_HELLO_QUERY;
    gb(data, 1) = from;
    gb(data, 2) = to;
    if (additional != nullptr)
    {
        memcpy((byte*)data + 3, additional, add_sz);
    }
    return data;
}

void* mkHelloAnswer(byte from, byte to, const std::vector<byte> &neighbors, void *additional, byte add_sz)
{
    size_t pkg_size = 12 + neighbors.size();
    void *data = malloc(pkg_size);
    memset(data, 0, pkg_size);
    gb(data, 0) = PKG_HELLO_ANSWER;
    gb(data, 1) = from;
    gb(data, 2) = to;
    gb(data, 3) = (byte)neighbors.size();
    for (size_t i = 0; i < neighbors.size(); i++)
        gb(data, 4 + i) = neighbors[i];
    if (additional != nullptr)
    {
        memcpy((byte*)data + 4, additional, add_sz);
    }
    return data;
}

void* mkRouteQuery(byte from, byte to, unsigned short rid, byte ctr, route rts)
{
    size_t pkg_size = 6 + rts.size();
    void *data = malloc(pkg_size);
    memset(data, 0, pkg_size);
    gb(data, 0) = PKG_ROUTE_QUERY;
    gb(data, 1) = from;
    gb(data, 2) = to;
    gb(data, 3) = (byte)(rid >> 8);
    gb(data, 4) = (byte)(rid << 8);
    gb(data, 5) = ctr;
    for (size_t i = 0; i < rts.size(); i++)
        gb(data, 6 + i) = rts[i];
    return data;
}

void* mkHelloError(byte from, byte to, byte node)
{
    size_t pkg_size = 4;
    void *data = malloc(pkg_size);
    memset(data, 0, pkg_size);
    gb(data, 0) = PKG_HELLO_ERROR;
    gb(data, 1) = from;
    gb(data, 2) = to;
    gb(data, 3) = node;
    return data;
}

void* mkRouteAnswer(byte source, byte addr, byte msgSrc, unsigned short rid, byte ctr, route rts)
{
    size_t pkg_size = 7 + rts.size();
    void *data = malloc(pkg_size);
    gb(data, 0) = PKG_ROUTE_ANSWER;
    gb(data, 1) = source;
    gb(data, 2) = addr;
    gb(data, 3) = msgSrc;
    gb(data, 4) = (byte)(rid >> 8);
    gb(data, 5) = (byte)(rid << 8);
    gb(data, 6) = ctr;
    for (size_t i = 0; i < rts.size(); i++)
        gb(data, 7 + i) = rts[i];
    return data;
}

void* mkDataQuery(byte from, byte to, ushort dtp, byte ctr, route rts) {
    size_t pkg_size = 6 + rts.size();
    void *data = malloc(pkg_size);
    gb(data, 0) = PKG_DATA_QUERY;
    gb(data, 1) = from;
    gb(data, 2) = to;
    gb(data, 3) = (byte)(dtp >> 8);
    gb(data, 4) = (byte)(dtp << 8);
    gb(data, 5) = ctr;
    for (size_t i = 0; i < rts.size(); i++)
        gb(data, 6 + i) = rts[i];
    return data;
}

void* mkDataAnswer(byte from, byte to, ushort dtp, byte ctr, route rts) {
    size_t pkg_size = 6 + rts.size();
    void *data = malloc(pkg_size);
    gb(data, 0) = PKG_DATA_ANSWER;
    gb(data, 1) = from;
    gb(data, 2) = to;
    gb(data, 3) = (byte)(dtp >> 8);
    gb(data, 4) = (byte)(dtp << 8);
    gb(data, 5) = ctr;
    for (size_t i = 0; i < rts.size(); i++)
        gb(data, 6 + i) = rts[i];
    return data;
}

void *mkApplicationMessage(byte from, byte to, int fid, byte ppf, byte num, byte prior, bool last, route rts, std::vector<byte> data) {
    size_t pkg_size = 12 + rts.size() + data.size();
    void *d = malloc(pkg_size);
    gb(d, 0) = PKG_APPLICATION_MESSAGE;
    gb(d, 1) = from;
    gb(d, 2) = to;
    gb(d, 3) = fid >> 24;
    gb(d, 4) = (fid << 8) / 65536;
    gb(d, 5) = (fid << 16) / 256;
    gb(d, 6) = fid % 256;
    gb(d, 7) = ppf;
    
    gb(d, 8) = num;
    gb(d, 9) = prior;
    gb(d, 10) = (byte)rts.size();
    for (size_t i = 0; i < rts.size(); i++) {
        gb(d, 11 + i) = rts[i];
    }

    gb(d, 12 + rts.size()) = data.size();
    for (size_t i = 0; i < data.size(); i++) {
        gb(d, 13 + rts.size() + i) = data[i];
    }

    return d;

}



