#pragma once
#include "types.h"
#include <vector>

struct ResponseEvent {
    byte node;
    byte type;

    ResponseEvent(byte n, byte t): node(n), type(t) { }

    bool operator==(const ResponseEvent &ev) const {
        return node == ev.node && type == ev.type;
    }
};

template <class EventType>
class Timeouts {

    std::vector<EventType> list;
    std::vector<float> timeouts;

public:

    void add(const EventType &ev, float t) {
        list.push_back(ev);
        timeouts.push_back(t);
    }

    void remove(const EventType &ev) {
        for (size_t i = 0; i < list.size(); i++)
            if (list[i] == ev) {
                list.erase(list.begin() + i);
                timeouts.erase(timeouts.begin() + i);
            }
    }

    void step(float t) {
        for (size_t i = 0; i < timeouts.size(); i++)
            timeouts[i] -= t;
    }

    bool hasTimeouts() const {
        for (size_t i = 0; i < timeouts.size(); i++)
            if (timeouts[i] <= 0.0f) return true;
        return false;
    }

    EventType handle() {
        for (size_t i = 0; i < timeouts.size(); i++)
            if (timeouts[i] <= 0.0f) {
                EventType e = list[i];
                list.erase(list.begin() + i);
                timeouts.erase(timeouts.begin() + i);
                return e;
            }
        return ResponseEvent(0, 0xff);
    }
};


