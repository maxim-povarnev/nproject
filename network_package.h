#pragma once
#include "utility.h"
#include "network_connection.h"
#include "network_node.h"


/*!
 * \brief NetworkPackage - передаваемый пакет
 */
class NetworkPackage
{
    /*!
     * \brief connection
     * Соединение, по которому передается пакет
     */
    NetworkConnection *connection;
    /*!
     * \brief source
     * Узел, передающий данный пакет
     */
    NetworkNode *source;
    /*!
     * \brief dest
     * Узел назначения
     */
    NetworkNode *dest;
    /*!
     * \brief size
     * Размер пакета в байтах
     */
    unsigned size; // in bytes
    /*!
     * \brief data
     * Передаваемые данные
     */
    void *data;
    /*!
     * \brief progress
     * Прогресс передачи (от 0 до 1)
     */
    float progress;
    /*!
     * \brief type
     * Тип передаваемого пакета
     */
    byte type;

public:

    /*!
     * \brief src
     * \return Источник пакета
     */
    NetworkNode& src() const
    {
        return *source;
    }

    /*!
     * \brief dst
     * \return Узел назначения
     */
    NetworkNode& dst() const
    {
        return *dest;
    }

    /*!
     * \brief NetworkPackage
     * Параметризованный конструктор для создания пакета
     * \param c Соединение, по которому будет вестись передача
     * \param sr Узел-источник
     * \param ds Узел назначения
     * \param d Передаваемые данные
     * \param sz Размер передаваемого пакета в байтах
     */
    NetworkPackage(NetworkConnection *c, NetworkNode *sr, NetworkNode *ds, void *d, unsigned sz)
    {
        connection = c;
        source = sr;
        dest = ds;
        data = d;
        size = sz;
        progress = 0.0f;
        type = ((byte*)d)[0];
    }

    /*!
     * \brief advance
     * Увеличить прогресс передачи пакета.
     * Если передача завершена, освободить соединение и передать
     * данные назначенному узлу.
     * \param rat Увеличиваемая доля прогресса передачи
     */
    void advance(float rat)
    {
        progress += rat;
        if (progress >= 1.0f)
        {
            if (connection != nullptr) connection->free(false);
            if (source != nullptr) {  source->setSending(false); }
            if (dest != nullptr && data != nullptr) { dest->setReceiving(false);
                    dest->recv(source->number(), data, size);
            }
        }
    }

    /*!
     * \brief nc
     * \return Соединение, по которому ведется передача
     */
    NetworkConnection *nc()
    {
        return connection;
    }

    /*!
     * \brief done
     * \return Статус завершения передачи
     */
    inline bool done()
    {
        return progress >= 1.0f;
    }

    /*!
     * \brief bytes
     * Получить размер передаваемого пакета в байтах
     */
    inline unsigned bytes()
    {
        return size;
    }

    /*!
     * \brief dptr
     * \return Указатель на передаваемые данные
     */
    inline void *dptr() const {
        return data;
    }

    /*!
     * \brief pos
     * \return Положение пакета при его отображении в пользовательском интерфейсе
     */
    v2f pos()
    {
        return lerp(source->position(), dest->position(), progress);
    }
    /*!
     * \brief getType
     * \return Код, указывающий на тип передаваемого пакета
     */
    byte getType() {
        return type;
    }

    float getProgress() const {
        return progress;
    }


    void drop()
    {
        free(data);
        data = nullptr;
    }

    bool dropped() const {
        return data == nullptr;
    }
};
