#include <deque>
#include <fstream>
#pragma once

/*!
 * \brief LogEntry
 * Структура для хранения записи лога
 */
struct LogEntry {
    /*!
     * \brief time
     * Время доставки пакета
     */
    float time;
    /*!
     * \brief type
     * Тип пакета
     */
    byte type;
    /*!
     * \brief from
     * Источник
     */
    byte from;
    /*!
     * \brief to
     * Получатель
     */
    byte to;

    LogEntry(float t, byte tp, byte f, byte _to) {
        time = t;
        type = tp;
        from = f;
        to = _to;
    }
};

/*!
 * \brief Logger
 * Класс, выполняющий логгирование пакетов и запись в файл
 */
class Logger {
    /*!
     * \brief list
     * Список пакетов
     */
    std::deque<LogEntry> list;
    /*! \brief pkgType
     * Типы используемых пакетов
     */
    std::vector<const char*> pkgType =
    {"ACCESS_QUERY",
        "ACCESS_ANSWER", "HELLO_QUERY", "HELLO_ANSWER",
        "HELLO_ERROR", "ROUTE_QUERY", "ROUTE_ANSWER",
        "ROUTE_ERROR", "DATA_QUERY", "DATA_ANSWER",
        "DATA_ERROR", "STATUS_MESSAGE", "COMMAND_MESSAGE",
        "APPLICATION_MESSAGE"};
public:

    /*!
     * \brief add
     * Добавить пакет в лог
     * \param e
     */
    void add(const LogEntry &e) {
        list.push_back(e);
    }

    /*!
     * \brief clear
     * Очистка списка пакетов в логе
     */
    void clear() {
        list.clear();
    }

    /*!
     * \brief save
     * Сохранить лог в файл
     * \param fn Имя файла
     */
    void save(const char *fn) {
        std::ofstream output(fn);
        output << "time | type | from | to\n";
        for (size_t i = 0; i < list.size(); i++) {
            output << list[i].time << ' ';
            output << pkgType[list[i].type] << ' ';
            output << (int)list[i].from << ' ';
            output << (int)list[i].to << '\n';
        }
        output.close();
    }
};
