#pragma once
#include <QMessageBox>
#include "v2f.h"
#include <vector>
#include <QDebug>
#include <deque>
#include "timerswitch.h"
#include "types.h"
#include "timeouts.h"



/*!
 * \brief RawPkg
 * Представление пакета в неструктурированном виде.
 * Используется для формирования очереди пакетов сетевого агента.
 */
class RawPkg {
public:
    /*!
     * \brief data
     * Передаваемые данные
     */
    void *data;
    /*!
     * \brief size
     * Размер пакета
     */
    size_t size;
    /*!
     * \brief dest
     * Логический адрес узла-получателя
     */
    byte dest;

    /*!
     * \brief RawPkg
     * Параметризованный конструктор для формирования неструктурированного пакета
     * \param dst Узел-получатель
     * \param dt Передаваемые данные
     * \param sz Размер пакета
     */
    RawPkg(byte dst, void *dt, size_t sz)
    {
        dest = dst;
        data = dt;
        size = sz;
    }
};

/*!
 * \brief NetworkAddr
 * Класс, хранящий уникальный и логический адреса узла в сети,
 * а так же его приоритет. Используется для задания таблицы
 * известных узлов в сетевом агенте и их идентификации.
 */
class NetworkAddr {
public:
    /*!
     * \brief ukey
     * Уникальный адрес узла сети
     */
    uint64 ukey;
    /*!
     * \brief addr
     * Логический адрес, присвоенный узлу сети
     */
    byte addr;
    /*!
     * \brief prior
     * Приоритет узла
     */
    byte prior;

    /*!
     * \brief NetworkAddr
     * Параметризованный конструктор
     * \param _addr Логический адрес узла в сети
     */

    NetworkAddr(byte _addr)
    {
        ukey = 0;
        addr = _addr;
        prior = 0;
    }
};
/*!
 * \brief NetworkPath
 * Класс, представляющий собой модель двуступенчатого маршрута
 */
class NetworkPath {

public:
    /*!
     * \brief neighbor
     * Логический адрес смежного узла
     */
    byte neighbor;
    /*!
     * \brief neighbor2
     * Логический адрес узла, доступ к которому может
     * быть осуществлен через узел neighbor
     */
    byte neighbor2;

    /*!
     * \brief NetworkPath
     * Параметризованный конструктор
     * \param n1 Логический адрес смежного узла
     * \param n2 Логический адрес узла, доступного через смежный
     */
    NetworkPath(byte n1, byte n2)
    {
        neighbor = n1;
        neighbor2 = n2;
    }

    /*!
     * \brief operator ==
     * Перегрузка оператора сравнения для двуступенчатого маршрута
     * \param np Двуступенчатый маршрут, с которым производится сравнение
     * \return Результат сравнения
     */
    bool operator==(const NetworkPath &np) const {
        return neighbor == np.neighbor && neighbor2 == np.neighbor2;
    }

};



/*!
 * \brief mkAccessQuery
 * Сформировать пакет типа AccessQuery
 * \param from Узел-источник
 * \param to Узел-получатель
 * \param additional Дополнительные данные
 * \param add_sz Размер дополнительных данных в байтах
 * \return Указатель на буфер, где сформирован пакет
 */
void* mkAccessQuery(byte from, byte to, void *additional = nullptr, byte add_sz = 0);
/*!
 * \brief mkAccessAnswer
 * Сформировать пакет типа AccessAnswer
 * \param from Узел-источник
 * \param to Узел-получатель
 * \param neighbors Список смежных узлов, передаваемых при ответе
 * \param additional Дополнительные данные
 * \param add_sz Размер дополнительных данных в байтах
 * \return Указатель на буфер, где сформирован пакет
 */
void* mkAccessAnswer(byte from, byte to, const std::vector<byte> &neighbors, void *additional = nullptr, byte add_sz = 0);
/*!
 * \brief mkHelloQuery
 * Сформировать пакет типа AccessAnswer
 * \param from Узел-источник
 * \param to Узел-получатель
 * \param additional Дополнительные данные
 * \param add_sz Размер дополнительных данных в байтах
 * \return Указатель на буфер, где сформирован пакет
 */
void* mkHelloQuery(byte from, byte to, void *additional = nullptr, byte add_sz = 0);
/*!
 * \brief mkHelloAnswer
 * Сформировать пакет типа HelloAnswer
 * \param from Узел-источник
 * \param to Узел-получатель
 * \param neighbors Список смежных узлов, передаваемых в ответе
 * \param additional Дополнительные данные
 * \param add_sz Размер дополнительных данных в байтах
 * \return Указатель на буфер, где сформирован пакет
 */
void* mkHelloAnswer(byte from, byte to, const std::vector<byte> &neighbors, void *additional = nullptr, byte add_sz = 0);
/*!
 * \brief mkHelloError
 * Сформировать пакет типа HelloError
 * \param from Узел-отправитель
 * \param to Узел-получатель
 * \param node Логический адрес вершины, для которой произошла ошибка передачи
 * \return Указатель на буфер, где сформирован пакет
 */
void* mkHelloError(byte from, byte to, byte node);
/*!
 * \brief mkRouteQuery
 * Сформировать пакет типа RouteQuery
 * \param from Узел-отправитель
 * \param to Узел-получатель
 * \param rid Уникальный идентификатор запроса маршрута
 * \param ctr Счетчик переходов
 * \param rts Список пройденных узлов
 * \return Указатель на буфер, где сформирован пакет
 */
void* mkRouteQuery(byte from, byte to, unsigned short rid, byte ctr, route rts);

/*!
 * \brief mkRouteAnswer
 * Сформировать пакет типа RouteAnswer
 * \param source Источник
 * \param addr Получатель
 * \param msgSrc Узел, впервые сгенерировавший сообщение RouteAnswer
 * \param rid Уникальный идентификатор запроса маршрута
 * \param ctr Счетчик переходов
 * \param rts Список пройденных узлов
 * \return Указатель на буфер, где сформирован пакет
 */
void* mkRouteAnswer(byte source, byte addr, byte msgSrc, unsigned short rid, byte ctr, route rts);

void* mkDataQuery(byte from, byte to, ushort dtp, byte ctr, route rts);
void* mkDataAnswer(byte from, byte to, ushort dtp, byte ctr, route rts);
void* mkApplicationMessage(byte from, byte to, int fid, byte ppf, byte num, byte prior, bool last, route rts, std::vector<byte> data);


/*!
 * \brief NetworkAgent
 * Класс сетевого агента, который определяет протокол обмена данными между узлами.
 */
class NetworkAgent {
public:

    std::deque<RawPkg> scheduledPkgs;
    std::deque<byte> transfers;

    /*!
     * \brief enqueue
     * Добавить пакет в очередь на отправку
     * \param dst Узел-получатель
     * \param data Передаваемые данные
     * \param sz Размер пакета
     * \param highPrior Высокий приоритет (добавить в начало очереди). false по умолчанию
     */
    void enqueue(byte dst, void *data, size_t sz, bool highPrior = false)
    {
        if (!highPrior)
        {
            scheduledPkgs.push_back(RawPkg(dst, data, sz));
        }
        else
        {
            scheduledPkgs.push_front(RawPkg(dst, data, sz));
        }
    }

    void addTransfer(void *data, size_t size) {
        for (size_t i = 0; i < size; i++)
            transfers.push_back(gb(data, i));
    }

    /*!
     * \brief number
     * Логический адрес узла в сети
     */
    byte number;

    /*!
     * \brief isSending
     * Индикатор занятости агента передачей данных
     */
    bool isSending = false;
    /*!
     * \brief isReceiving
     * Индикатор занятости агента получением данных
     */
    bool isReceiving = false;

    /*!
     * \brief time
     * Время работы модели (в секундах)
     */
    float *time;
    /*!
     * \brief pos
     * Разположение узла, на котором функционирует сетевой агент
     */
    v2f *pos;
    /*!
     * \brief spd
     * Скорость перемещения узла, на котором функционирует сетевой агент
     */
    v2f *spd;
    /*!
     * \brief inp_data
     * Указатель на буфер получаемых данных
     */
    void *inp_data;
    /*!
     * \brief out_data
     * Указатель на буфер передаваемых данных
     */
    void *out_data;
    /*!
     * \brief inp_size
     * Размер получаемых данных в байтах
     */
    unsigned inp_size;
    /*!
     * \brief out_size
     * Размер передаваемых данных в байтах
     */
    unsigned out_size;
    /*!
     * \brief inp_addr
     * Логический адрес узла, от которого получены данные
     */
    unsigned inp_addr;
    /*!
     * \brief out_addr
     * Логический адрес узла, которому отправляются данные
     */
    unsigned out_addr;

    /*!
     * \brief center
     * Логический адрес главной вершины
     */
    byte center = 0xff;
    /*!
     * \brief source
     * Логический адрес вершины, отправляющей данные
     */
    byte source = 0xff;
    /*!
     * \brief dest
     * Логический адрес вершины, получающей данные
     */
    byte dest = 0xff;

    /*!
     * \brief rids
     * Список ранее получаемых идентификаторов ROUTE_QUERY
     */
    std::vector<unsigned short> rids;

    // --- ------  -----

    /*!
     * \brief net_table
     * Таблица известных узлов сети.
     * Включает в себя уникальные и логические адреса известных узлов,
     * а так же их приоритет.
     */
    std::vector<NetworkAddr> net_table;
    /*!
     * \brief listNeighbor
     * Список логических адресов узлов, с которыми имеется непосредственная связь
     */
    std::vector<byte> listNeighbor;
    /*!
     * \brief listNeighbor2
     * Список известных двуступенчатых маршрутов
     */
    std::vector<NetworkPath> listNeighbor2;

    std::vector<route> listRoutes;

    // ---- -- - -- - --

    /*!
     * \brief NetworkAgent
     * Конструктор сетевого агента по умолчанию.
     */
    NetworkAgent(): inp_data(nullptr), out_data(nullptr), handshakeTimer(5.0f), helloTimer(7.0f) {

        handshakeTimer.activate();
    }

    /*!
     * \brief initTable
     * Инициализация таблицы известных узлов
     */
    void initTable() {
        for (byte i = 1; i < 17; i++)
        {
            if (i == number) continue;
           net_table.push_back(NetworkAddr(i));
        }
    }

    /*!
     * \brief handshakeTimer
     * Таймер для процедуры handshake
     */
    TimerSwitch handshakeTimer;

    /*!
     * \brief helloTimer
     * Таймер для процедуры hello
     */
    TimerSwitch helloTimer;
    Timeouts<ResponseEvent> timeouts;
    bool mutexDataQuery = false;
    bool dataReady = false;


    void addTimeouts(RawPkg pkg)
    {
        void *data = pkg.data;
        if (gb(data, 0) == PKG_HELLO_QUERY)
            timeouts.add(ResponseEvent(gb(data, 2), PKG_HELLO_ANSWER), HELLO_OK_TIME);
        if (gb(data, 0) == PKG_DATA_QUERY) {
            timeouts.add(ResponseEvent(0, PKG_DATA_ANSWER), DATA_ANSWER_TIME);
        }
    }

    void handleTimeout(ResponseEvent e) {
        // таймаут HELLO_ANSWER
        if (e.type == PKG_HELLO_ANSWER) {
            // рассылка HELLO_ERROR
            for (size_t i = 0; i < listNeighbor.size(); i++)
            {
                if (listNeighbor[i] == e.node) continue;
                enqueue(listNeighbor[i], mkHelloError(number, listNeighbor[i], e.node), 4, true);
            }
        }

        if (e.type == PKG_DATA_ANSWER) {
            mutexDataQuery = false;
            // handle data answer timeout
        }

        if (e.type == PKG_ROUTE_ANSWER) {
            mutexDataQuery = false;
            // handle route answer timeout
        }
    }

    /*!
     * \brief step
     * Выполняет шаг моделирования
     */
    void step(float ts) {
        // если имеются входные данные
        if (inp_data != nullptr) {
            recv(); // вызвать функцию получения данных
            free(inp_data); // освободить память
            inp_data = nullptr; // установить указатель в нулевой
        }

        // отправка очередного запланированного пакета
        if (canSend() && scheduledPkgs.size() != 0)
        {
            RawPkg pkg = scheduledPkgs.front();
            send(pkg.dest, pkg.data, pkg.size);
            addTimeouts(pkg);
            scheduledPkgs.pop_front();
        }

        // периодическая рассылка ACCESS_QUERY
        handshakeTimer.step(ts);
        if (handshakeTimer.isActive()) {
            handshakeTimer.turnOff();
            for (size_t i = 0; i < net_table.size(); i++) {
                if (std::find(listNeighbor.begin(), listNeighbor.end(), net_table[i].addr) == listNeighbor.end()) {
                    enqueue(net_table[i].addr, mkAccessQuery((byte)number, net_table[i].addr), 11);
                }
            }
        }

        // периодическая рассылка HELLO_QUERY
        helloTimer.step(ts);
        if (helloTimer.isActive()) {
            helloTimer.turnOff();
            for (size_t i = 0; i < net_table.size(); i++) {
                if (std::find(listNeighbor.begin(), listNeighbor.end(), net_table[i].addr) != listNeighbor.end()) {
                    enqueue(net_table[i].addr, mkHelloQuery((byte)number, net_table[i].addr), 11);
                }
            }
        }

        timeouts.step(ts);
        // Проверка истечения времени ожидания ответа
        while (timeouts.hasTimeouts()) {
            ResponseEvent e = timeouts.handle();
            handleTimeout(e);
        }

        // передача данных
        if (source == number && dest != 0xff && transfers.size() != 0 && !mutexDataQuery) {
            // попытка найти узел назначения в списке соседей
            bool found = false;
            auto vn = std::find(listNeighbor.begin(), listNeighbor.end(), dest);
            // получатель найден в списке соседей
            if (vn != listNeighbor.end()) {
                route v;
                v.push_back(number);
                v.push_back(*vn);
                // отправка пакета data query - запрос готовности получения данных
                enqueue(*vn, mkDataQuery(number, dest, 0, v.size(), v), 6 + v.size());
                found = true;
                mutexDataQuery = true; // остановить повторную отправку data query
            }

            // если узел не найден в списке соседей, искать в списке двуступенчатых маршрутов
            if (!found)
            {
                for (size_t i = 0; i < listNeighbor2.size(); i++) {
                    // если узел найден в списке двуступенчатых маршрутов
                    if (listNeighbor2[i].neighbor2 == dest) {
                        route v;
                        v.push_back(number);
                        v.push_back(listNeighbor2[i].neighbor);
                        v.push_back(listNeighbor2[i].neighbor2);
                        enqueue(listNeighbor2[i].neighbor, mkDataQuery(number, dest, 0, v.size(), v), 6 + v.size());
                        found = true;
                        mutexDataQuery = true;
                        break;
                    }
                }
            }

            if (!found) {
                for (size_t i = 0; i < listRoutes.size(); i++) {
                    for (size_t j = 0; i < listRoutes.size() && j < listRoutes[i].size(); j++) {
                        if (listRoutes[i][j] == dest) {
                            route v;
                            for (size_t k = 0; k <= j; k++)
                                v.push_back(listRoutes[i][k]);
                            enqueue(v[1], mkDataQuery(number, dest, 0, v.size(), v), 6 + v.size());
                            found = true;
                            mutexDataQuery = true;
                            j = listRoutes[i].size();
                            i = listRoutes.size();
                        }
                    }
                }
            }

            if (!found) {

                mutexDataQuery = true;
                for (size_t i = 0; i < listNeighbor.size(); i++) {
                    route v; v.push_back(number);
                    enqueue(listNeighbor[i], mkRouteQuery(number, dest, (ushort)rand(), 1, v), 6 + v.size());
                }
            }
        }
    }

    /*!
     * \brief recv
     * Метод получения данных сетевым агентом
     */
    void recv() {

        // если получен пакет ACCESS_QUERY
        if (gb(inp_data, 0) == PKG_ACCESS_QUERY)
        {
            byte from = gb(inp_data, 1); // источник пакета
            //byte to = gb(inp_data, 2); // получатель пакета

            bool net_found = false; // найден ли узел from в таблице

            for (size_t i = 0; i < net_table.size(); i++) // поиск узла в таблице
                if (net_table[i].addr == from) {net_found = true; break; }

            if (!net_found) return; // если узел не найден - завершение

            // если узел еще не присутствует в списке соседей,
            if (std::find(listNeighbor.begin(), listNeighbor.end(), from) == listNeighbor.end())
                listNeighbor.push_back(from); // добавить в список соседей

            // отправить ACCESS_ANSWER
            enqueue((unsigned)from, mkAccessAnswer((byte)number, from, listNeighbor), 4 + listNeighbor.size(), true);
        }

        // Если получен пакет ACCESS_ANSWER
        if (gb(inp_data, 0) == PKG_ACCESS_ANSWER)
        {
            //hsResponseWait = true; // приостановить процедуру handshake на один шаг
            byte from = gb(inp_data, 1); // узел-отправитель
            byte to = gb(inp_data, 2); // узел-получатель
            byte nc = gb(inp_data, 3); // количество узлов-соседей узла from

            std::vector<byte> n; // контейнер для извлечения узлов-соседей из содержимого пакета
            for (size_t i = 0; i < nc; i++)
                n.push_back(gb(inp_data, 4 + i)); // извлечь узлы в n

            bool net_found = false; // найден ли узел в таблице
            if ((byte)number != to) return; // если адресатом должен быть другой узел - завершение

            for (size_t i = 0; i < net_table.size(); i++) // поиск узла в таблице
                if (net_table[i].addr == from) {net_found = true; break; }

            // если не найден - завершение
            if (!net_found) return;

            // если узел не присутствует в списке соседей - добавить
            if (std::find(listNeighbor.begin(), listNeighbor.end(), from) == listNeighbor.end())
                listNeighbor.push_back(from);

            // добавить двуступенчатые маршруты
            for (size_t i = 0; i < nc; i++)
            {
                if (std::find(listNeighbor2.begin(), listNeighbor2.end(), NetworkPath(from, n[i])) == listNeighbor2.end()
                        && n[i] != number)
                    listNeighbor2.push_back(NetworkPath(from, n[i]));
            }

        }

        // Если получен пакет HELLO_QUERY
        if (gb(inp_data, 0) == PKG_HELLO_QUERY)
        {
            byte from = gb(inp_data, 1); // узел-отправитель
            //byte to = gb(inp_data, 2); // узел-получатель

            bool net_found = false; // найден ли узел в таблице

            for (size_t i = 0; i < net_table.size(); i++) // поиск узла в таблице
                if (net_table[i].addr == from) {net_found = true; break; }

            if (!net_found) return; // если не найден - завершение

            // добавление узла в список соседей (при необходимости)
            if (std::find(listNeighbor.begin(), listNeighbor.end(), from) == listNeighbor.end())
                listNeighbor.push_back(from);

            // формирование ответа HELLO_ANSWER
            enqueue((unsigned)from, mkHelloAnswer((byte)number, from, listNeighbor), 12 + listNeighbor.size(), true);

        }
        // Если получен пакет HELLO_ANSWER
        if (gb(inp_data, 0) == PKG_HELLO_ANSWER)
        {

            byte from = gb(inp_data, 1); // узел-отправитель
            byte to = gb(inp_data, 2); // узел-получатель
            byte nc = gb(inp_data, 3); // количество узлов-соседей узла from

            std::vector<byte> n;
            for (size_t i = 0; i < nc; i++)
                n.push_back(gb(inp_data, 4 + i)); // извлечь из пакета смежные узлы from

            bool net_found = false;

            for (size_t i = 0; i < net_table.size(); i++)
                if (net_table[i].addr == from) {net_found = true; break; }

            if (!net_found) return;

            if ((byte)number != to) return;

            if (std::find(listNeighbor.begin(), listNeighbor.end(), from) == listNeighbor.end())
                listNeighbor.push_back(from);

            for (size_t i = 0; i < nc; i++)
            {
                if (std::find(listNeighbor2.begin(), listNeighbor2.end(), NetworkPath(from, n[i])) == listNeighbor2.end()
                        && n[i] != number)
                    listNeighbor2.push_back(NetworkPath(from, n[i]));
            }

            timeouts.remove(ResponseEvent(from, PKG_HELLO_ANSWER));
        }



        // Если получен пакет ROUTE_QUERY
        if (gb(inp_data, 0) == PKG_ROUTE_QUERY)
        {
            byte from = gb(inp_data, 1); // узел-отправитель
            byte to = gb(inp_data, 2); // узел-получатель
            unsigned short rid = gb(inp_data, 3) * 256 + gb(inp_data, 4); // id запроса
            byte ctr = gb(inp_data, 5); // размер пройденного пути
            route v; // список пройденных маршрутов

            // если TTL превышено - уничтожить пакет
            if (ctr > TTL)
                return;

            // нет подходящих узлов для ретрансляции (уничтожение пакета)
            if (listNeighbor.size() == 1)
                return;

            // отброс устаревших запросов маршрутизации
            if (std::find(rids.begin(), rids.end(), rid) != rids.end())
                return;
            else rids.push_back(rid);

            // извлечение пройденного маршрута из пакета
            for (size_t i = 0; i < ctr; i++)
                v.push_back(gb(inp_data, 6 + i));

            // обнаружена петля - уничтожить пакет
            if (std::find(v.begin(), v.end(), number) != v.end())
                return;

            // -- поиск получателя в списке соседей
            auto n1 = std::find(listNeighbor.begin(), listNeighbor.end(), to);
            if (n1 != listNeighbor.end()) { // получатель найден в списке соседей
                ctr += 2;
                v.push_back(number);
                v.push_back(to);
                enqueue(v[v.size() - 3], mkRouteAnswer(from, to, number, rid, ctr, v), 7 + v.size(), true);
                return;
            }

            ctr++;
            v.push_back(number);

            for (size_t i = 0; i < listNeighbor.size(); i++)
            {
                if (std::find(v.begin(), v.end(), listNeighbor[i]) != v.end()) continue;
                void *pkg = mkRouteQuery(from, to, rid, ctr, v);
                size_t pkg_sz = 6 + v.size();
                enqueue(listNeighbor[i], pkg, pkg_sz, true);
            }
        }

        if (gb(inp_data, 0) == PKG_ROUTE_ANSWER)
        {
            byte from = gb(inp_data, 1); // узел-отправитель
            byte to = gb(inp_data, 2); // узел-получатель
            byte msgSrc = gb(inp_data, 3);
            unsigned short rid = gb(inp_data, 4) * 256 + gb(inp_data, 5); // id запроса
            byte ctr = gb(inp_data, 6); // размер пройденного пути
            route v; // список пройденных маршрутов
            for (size_t i = 0; i < ctr; i++)
                v.push_back(gb(inp_data, 7 + i));

            if (from == number) // ответ вернулся к исходному узлу
            {
                qDebug() << "route found";
                listRoutes.push_back(v);
                mutexDataQuery = false;
                return;
            }

            // если узел не содержится в пути - отбросить пакет
            if (std::find(v.begin(), v.end(), number) == v.end()) return;

            // поиск очередного узла-получателя при обратном распространении
            for (size_t i = 0; i < v.size(); i++)
            {
                if (v[i] == number)
                    enqueue(v[i - 1], mkRouteAnswer(from, to, msgSrc, rid, ctr, v), 7 + v.size(), true);
            }
        }

        if (gb(inp_data, 0) == PKG_DATA_QUERY)
        {
            byte from = gb(inp_data, 1);
            byte to = gb(inp_data, 2);
            unsigned short dtp = gb(inp_data, 3) * 256 + gb(inp_data, 4); // id запроса
            byte ctr = gb(inp_data, 5);
            route rts;
            for (size_t i = 0; i < ctr; i++)
                rts.push_back(gb(inp_data, 6 + i));

            if (to == number) {
                std::reverse(rts.begin(), rts.end());
                enqueue(rts[1], mkDataAnswer(number, from, dtp, rts.size(), rts),  6 + rts.size());
            }
            else
            {
                for (size_t i = 0; i < rts.size(); i++)
                {
                    if (number == rts[i])
                        enqueue(rts[i + 1], mkDataQuery(from, to, dtp, ctr, rts), 6 + rts.size());
                }
            }

        }

        if (gb(inp_data, 0) == PKG_DATA_ANSWER) {

            byte from = gb(inp_data, 1);
            byte to = gb(inp_data, 2);
            ushort dtp = gb(inp_data, 3) * 256 + gb(inp_data, 4);
            byte ctr = gb(inp_data, 5);
            route rts;
            for (size_t i = 0; i < ctr; i++)
                rts.push_back(gb(inp_data, 6 + i));


            if (to == number)
            {
                std::reverse(rts.begin(), rts.end());
                while (transfers.size() != 0) {
                    std::vector<byte> dt;
                    size_t sz = transfers.size() > 255 ? 255 : transfers.size();
                    for (size_t i = 0; i < sz; i++)
                    {
                        dt.push_back(transfers.front());
                        transfers.pop_front();
                    }

                    enqueue(rts[1], mkApplicationMessage(number, from, rand(), 10, 0, 0, 0, rts, dt), 12 + rts.size() + dt.size());
                }
            }
            else
            {
                for (size_t i = 0; i < rts.size(); i++)
                    if (rts[i] == number)
                        enqueue(rts[i + 1], mkDataAnswer(from, to, dtp, ctr, rts), 6 + rts.size());
            }

        }

        if (gb(inp_data, 0) == PKG_APPLICATION_MESSAGE) {

            byte from = gb(inp_data, 1);
            byte to = gb(inp_data, 2);
            int fid = gb(inp_data, 3) * 155017216 + gb(inp_data, 4) * 65536 + gb(inp_data, 5) * 256 + gb(inp_data, 6);
            byte ppf = gb(inp_data, 7);
            byte num = gb(inp_data, 8);
            byte prior = gb(inp_data, 9);
            byte cnt = gb(inp_data, 10);
            route rts;
            for (size_t i = 0; i < cnt; i++)
                rts.push_back(gb(inp_data, 11 + i));
            byte dts = gb(inp_data, 12 + rts.size() );
            std::vector<byte> data;
            for (size_t i = 0; i < dts; i++)
                data.push_back(gb(inp_data, 13 + rts.size() + i));

            if (to == number)
                return;

            for (size_t i = 0; i < rts.size(); i++)
                if (number == rts[i])
                    enqueue(rts[i+1], mkApplicationMessage(from, to, fid, ppf, num, prior, 0, rts, data), 12 + rts.size() + data.size(), false);
        }
    }

    /*!
     * \brief send
     * Метод отправки данных от сетевого агента узлу
     * \param addr
     * Логический адрес узла-получателя
     * \param data
     * Указатель на буфер передаваемых данных
     * \param size
     * Размер передаваемых данных в байтах
     */
    void send(unsigned addr, void *data, unsigned size) {
        if (out_data != nullptr || !canSend()) {
            free(data);
            return;
        }
        out_addr = addr;
        out_data = data;
        out_size = size;
    }

    // указать, что агент на данный момент отправляет данные или свободен для отправки
    void setSending(bool state = true) {
        isSending = state;
    }
    // указать, что агент на данный момент получает данные или свободен для приема
    void setReceiving(bool state = true) {
        isReceiving = state;
    }

    // может ли агент отправлять данные
    inline bool canSend() const {
        return !isSending;
    }

    // может ли агент получать данные
    inline bool canReceive() const {
        return !isReceiving;
    }



    /*!
     * \brief TTL
     * Ограничение на максимальное количество "хопов"
     */
    byte TTL = 8;


    /*!
     * \brief HND_TIME
     * интервал времени, с которым узел проводит процедуру handshake
      («рукопожатия»), для отслеживания соседних узлов.
     */
    float HND_TIME = 5.0f;

    /*!
     * \brief HND_ANSWER_TIME
     *  максимальное время ответа, которое ожидает узел от
    соседнего узла во время процедуры handshake («рукопожатия»).
     */
    float HND_ANSWER_TIME = 1.0f;

    /*!
     * \brief HELLO_TIME
     * Интервал времени, определяющий частоту проверки узлом доступности своих соседей
     */
    float HELLO_TIME = 7.0f;
    /*!
     * \brief HELLO_OK_TIME
     * Узел считается недоступным, если ответ HELLO_ANSWER не был получен в течении времени HELLO_OK_TIME
     */
    float HELLO_OK_TIME = 1.0f;
    /*!
     * \brief DATA_ANSWER_TIME
     * Время ожидания подтверждения готовности приема данных узлом
     */
    float DATA_ANSWER_TIME = 3.0f;
    /*!
     * \brief REPEATED_DQUERY_TIME
     *  время, через которое узел-источник данных повторно
    отправляет запрос узлу-приемнику данных на передачу в случае, если приемник при
    предыдущем запросе не был готов к обмену.

     */
    float REPEATED_DQUERY_TIME = 6.0f;
    /*!
     * \brief DATA_TRANSFERRED_TIME
     * максимальное время, которое узел-источник данных
    ожидает сообщение подтверждения приема фрейма от узла-приемника.
     *
     */
    float DATA_TRANSFERRED_TIME = 10.0f;
    /*!
     * \brief DATA_REPEATED_TIME
     * максимальное время, которое узел-приемник данных
    ожидает повторной пересылки потерянных пакетов от узла-источника данных. Если
    потерянные пакеты за это время не получены, то фрейм считается сбойным и удаляется.
     */
    float DATA_REPEATED_TIME = 5.0f;
    /*!
     * \brief ROUTE_SEARCH_TIME
     * максимально время, отводимое узлу-источнику данных на
    поиск маршрута к узлу-приемнику данных. По истечении этого времени считается, что
    маршрут не существует и узел-приемник не доступен из подсети, где располагается узел-
    источник.
     */
    float ROUTE_SEARCH_TIME = 3.0f;
    /*!
     * \brief ACTUAL_ROUTE_TIME
     * время, которое найденный маршрут сохраняется в списке
    найденных маршрутов узла. По истечении этого времени маршрут между двумя узлами из
    списка удаляется, и его поиск инициируется заново. Если ACTUAL_ROUTE_TIME = 0, то
    маршрут в списке не будет обновляться, пока не возникнет ошибка при передачи пакетов по
    нему.
     */
    float ACTUAL_ROUTE_TIME = 0.0f;


};
