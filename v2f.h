#pragma once
#include <cmath>

// возведение в квадрат
inline float sqrf(float x)
{
    return x * x;
}

/*!
    \brief Двумерный вектор с вещественными компонентами (float)

    Данный класс используется для работы с координатами узлов.
    Поддерживается умножение на скаляр, сложение / вычитание
    векторов и рассчет растояния.
*/
class v2f
{
public:

    /*!
     * \brief x, y Компоненты вектора
     */
    float x, y;

    /*!
     * \brief zero
     * статическое поле, равное нулевому вектору
     */
    const static v2f zero;

    /*!
     * \brief v2f
     * Конструктор по умолчанию, создающий нулевой вектор
     */
    v2f(): x(0), y(0) { }

    /*!
     * \brief v2f
     * Параметризованный конструктор
     * \param ax, ay Координаты создаваемого вектора
     */
    v2f(float ax, float ay): x(ax), y(ay) { }

    /*!
     * \brief operator *
     * Перегрузка оператора умножения вектора на скалярную величину
     * \param m скалярная величина, на которую выполняется умножение
     * \return Вектор, умноженный на указанную скалярную величину
     */
    v2f operator*(float m) {
        return v2f(m * x, m * y);
    }

    /*!
     * \brief operator +=
     * Перегрузка оператора сложения с присваиванием
     * \param v Вектор, на который требуется увеличить исходный
     * \return  Вектор, увеличенный на полученную векторную величину
     */
    v2f& operator+=(const v2f& v) {
        x += v.x;
        y += v.y;
        return *this;
    }
    /*!
     * \brief operator +
     * Перегрузка оператора сложения двух векторов
     * \param v Второе слагаемое
     * \return Результат сложения
     */
    v2f operator+(const v2f &v) const
    {
        return v2f(x + v.x, y + v.y);
    }

    /*!
     * \brief operator -
     * Перегрузка оператора вычитания двух векторов
     * \param v Вычитаемое
     * \return Результат вычитания
     */
    v2f operator-(const v2f &v) const
    {
        return v2f(x - v.x, y - v.y);
    }


    /*!
     * \brief distanceTo
     * Метод нахождения расстояния между исходной точкой и заданной
     * \param ot Заданная точка
     * \return Расстояние (float)
     */
    float distanceTo(const v2f &ot) const
    {
        return sqrt(sqrf(x - ot.x) + sqrf(y - ot.y));
    }

};
